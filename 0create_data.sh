#!/bin/bash

odir=`pwd`
mdir=`dirname "$0"`
cd "$mdir"
mkdir data 2>/dev/null
cd data
#for fn in "blocked_hosts.rc blocked_uris.rc iframe_uris.rc referer_hosts.rc useragents.rc"

if [[ ! -f blocked_hosts.rc ]]; then
cat >blocked_hosts.rc <<EOT
# use : for regular expressions:
#:^*.\.googleapis.com$

*.facebook.com
*.facebook.net
*.twitter.com

# ssl checks
certs.opera.com
*.startssl.com
*.digicert.com
EOT
fi

if [[ ! -f blocked_uris.rc ]]; then
cat >blocked_uris.rc <<EOT
# only regexps!

^http://thedailywtf\.com/tizes/ads/
EOT
fi

if [[ ! -f iframe_uris.rc ]]; then
cat >iframe_uris.rc <<EOT
# only regexps!

P:^https?://[^/.]+\.livejournal\.com/talkpost_do.bml$
EOT
fi

if [[ ! -f referer_hosts.rc ]]; then
cat >referer_hosts.rc <<EOT
# NO regexps!

P:*.samlib.ru
EOT
fi

if [[ ! -f useragents.rc ]]; then
cat >useragents.rc <<EOT
# only regexps!
# - means 'don't change'
# format:
#
# useragent
# regexp
# regexp
#
# empty line (NOT COMMENT) delimits ua specs

#ie10: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)
#ff22: Mozilla/5.0 (Linux; en-US; rv:22.0) Gecko/20130405 Firefox/22.0

# others
Mozilla/5.0 (Linux; en-US; rv:22.0) Gecko/20130405 Firefox/22.0
^https?://
EOT
fi

if [[ ! -f ipv4_hosts.rc ]]; then
cat >ipv4_hosts.rc <<EOT
# IPv4-only hosts (no regexps), has priority over IPv6-only list
# *.example.com
EOT
fi

if [[ ! -f ipv6_hosts.rc ]]; then
cat >ipv6_hosts.rc <<EOT
# IPv6-only hosts (no regexps)
# *.example.com
EOT
fi


if [[ ! -f rewrite.rc ]]; then
cat >rewrite.rc <<EOT
# only regexps!
# will be done right after blocklists checking and before other checks
# use =:> to use 'Location:' header instead of direct rewriting
#^(https?://)its.me/hello$ ==> $1ketmar.no-ip.org/tmp/
#^(https?://)its.me/hi$ =:> $1ketmar.no-ip.org/tmp/
#^https://(pp\.vk\.me/c\d+/u\d+.*$) =:> http://$1
EOT
fi


cd ..


if [[ ! -d certs ]]; then
  mkdir certs
  cd crtgen
  sh certgen.sh
  cp *.pem ../certs
  cd ..
  echo " "
  echo "=========================================================="
  echo "add certs/cert.pem to your browser's root authorities list"
  echo "=========================================================="
fi

if [[ ! -d certs/generated ]]; then
  mkdir certs/generated
fi


cd "$odir"
