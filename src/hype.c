typedef struct hypehost_info_s {
  char *host;
  char *ipv6;
  struct hypehost_info_s *next;
} hypehost_info_t;


static hypehost_info_t *hypehosts = NULL;


static void hypehosts_clear (void) {
  while (hypehosts != NULL) {
    hypehost_info_t *h = hypehosts;
    hypehosts = hypehosts->next;
    free(h->host);
    free(h->ipv6);
    free(h);
  }
}


static const char *hypehost_find (const char *hostname) {
  for (const hypehost_info_t *hh = hypehosts; hh != NULL; hh = hh->next) {
    if (strcasecmp(hh->host, hostname) == 0) return hh->ipv6;
  }
  return NULL;
}


static void hypehosts_load (const char *fname) {
  FILE *fl;
  hypehosts_clear();
  fl = fopen(fname, "r");
  if (fl != NULL) {
    static char line[1024];
    while (fgets(line, sizeof(line), fl) != NULL) {
      char *spc;
      trim_str(line);
      if (!line[0] || line[0] == '#' || line[0] == ';') continue;
      for (spc = line; *spc && !isspace(*spc); ++spc) {}
      if (!spc[0]) continue;
      *spc++ = 0;
      while (*spc && isspace(*spc)) ++spc;
      if (!spc[0]) continue;
      if (!line[0]) continue;
      ptlogf("hype host: '%s' --> [%s]\n", line, spc);
      if (hypehost_find(line) == NULL) {
        // new host
        hypehost_info_t *hh = malloc(sizeof(*hh));
        hh->host = strdup(line);
        hh->ipv6 = strdup(spc);
        hh->next = hypehosts;
        hypehosts = hh;
      }
    }
    fclose(fl);
  }
}
