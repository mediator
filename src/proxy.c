#include "hazard.c"

//#define DUMP_DATA

#define skxabort  skclose


static const char *get_content_type (const char *fname) {
  static const struct {
    const char *ext;
    const char *type;
  } types[] = {
    {.ext=".js", .type="text/javascript"},
    {.ext=".css", .type="text/css"},
    {.ext=".html", .type="text/html"},
    {.ext=".ico", .type="image/png"},
    {.ext=".png", .type="image/png"},
  };
  if ((fname = strrchr(fname, '.')) == NULL) return "text/plain";
  for (size_t f = 0; f < ARRAYLEN(types); ++f) {
    if (strcasecmp(types[f].ext, fname) == 0) return types[f].type;
  }
  return "text/plain";
}


static void proxy_thread (void *arg) {
  coro_info_t *me = (coro_info_t *)arg;
  xbuf_t *clbuf = &me->bufs[0], *svbuf = &me->bufs[1];
  sock_t clifd, srvfd;
  int64_t content_length = -1;
  char ipstr[INET6_ADDRSTRLEN];
  http_method mt;
  char hostname[256], chostname[256], proto[16], portstr[16], *hn;
  int defport = 0; // default port?
  int port = 0, cport = 0, allow_iframe = 0, skip_host_blocks = 0;
  int allow_v4 = 1, allow_v6 = 1;
  // for getaddrinfo()
  //struct addrinfo *adrres;
  int gares;
  const char *new_ua = NULL;
  static char pathbuf[4096];
  const char *rewritten_uri = NULL;
  /* SSL shit */
  x509_crt *fake_cert = NULL; // malloc()ed
  //
  int fuckYoutube = 0;
  int isRZX = 0;

  int copy_and_parse_hostname (const void *str, int len) {
    const uint8_t *hh = (const uint8_t *)str;
    int p;
    if (len <= 0) len = 32768;
    for (p = 0; p < len && hh[p] > 32; ++p) {
      if (p >= sizeof(hostname)-1) return -1; // too long
      hostname[p] = tolower(hh[p]);
    }
    hostname[p] = 0;
    //
    if (strncmp(hostname, "fuck.", 5) == 0) {
      skip_host_blocks = 1;
      memmove(hostname, hostname+5, strlen(hostname+5)+1);
    } else {
      skip_host_blocks = 0;
    }
    //
    //skip_host_blocks = 0;
    while (buf_remove_header(clbuf->buf, "Host:") > 0) ;
    strcat(clbuf->buf, "Host: ");
    strcat(clbuf->buf, hostname);
    strcat(clbuf->buf, "\r\n");
    //
    if (hostname[0] == '[') {
      // IPv6
      char *pp = strrchr(hostname, ']');
      if (pp != NULL && pp[1] == ':' && pp[2] >= '0' && pp[2] <= '9') {
        // port
        //TODO: error checking!
        port = atoi((char *)(pp+2));
        pp[1] = 0;
        if (port < 1 || port > 65535) return -1;
      }
    } else {
      char *pp = strrchr(hostname, ':');
      if (pp != NULL && pp[1] >= '0' && pp[1] <= '9') {
        // port
        //TODO: error checking!
        port = atoi((char *)(pp+1));
        pp[0] = 0;
        if (port < 1 || port > 65535) return -1;
      }
    }
    if (!hostname[0]) return -1;
    //if (port < 1) port = 80;
    return 0;
  }

  int fix_query (void) {
    uint8_t *qaddr = (uint8_t *)clbuf->buf;
    while (*qaddr > 32) ++qaddr;
    while (*qaddr <= 32 && *qaddr != '\n') ++qaddr;
    if (*qaddr == '\n') return -1;
    //och = *eaddr;
    //*eaddr = 0;
    //ptlogf(":[%s]\n", (char *)qaddr);
    if (mt == MT_CONNECT) {
      // a very special beast
      if (copy_and_parse_hostname(qaddr, 0) < 0) return -1;
      strcpy(proto, "https");
      if (port == 0) port = 443;
      //if (port == 443) defport = 1;
    } else if (*qaddr == '/') {
      // no host in query string, try to find 'Host:' field
      char *hh = buf_find_header(clbuf->buf, "Host:");
      if (hh == NULL || *hh == '\r' || *hh == '\n') {
        if (chostname[0] == 0) return -1; // alas
        strcpy(hostname, chostname);
      } else {
        if (copy_and_parse_hostname(hh, 0) < 0) return -1;
      }
      strcpy(proto, "http");
      // no need to fix anything
      //if (port == 0) port = 80;
      //if (port == 80) defport = 1;
    } else {
      char pp[32];
      uint8_t *hp, *ee;
      //
      if (strncasecmp((const char *)qaddr, "http://", 7) == 0) {
        hp = qaddr+7;
        port = 80;
        //defport = 1;
        strcpy(proto, "http");
      } else if (strncasecmp((const char *)qaddr, "https://", 8) == 0) {
        hp = qaddr+8;
        port = 443;
        //defport = 1;
        strcpy(proto, "https");
      } else {
        return -1; // unknown protocol
      }
      //
      ee = (uint8_t *)strchr((char *)hp, '/');
      if (ee == NULL) return -1; // invalid query (no path), fuck it
      if (copy_and_parse_hostname(hp, ee-hp) < 0) return -1;
      //
      if (port) snprintf(pp, sizeof(pp), ":%d", port); else pp[0] = 0;
      memmove(qaddr, ee, strlen((char *)ee)+1);
    }
    if (port == 0) { port = 80; /*defport = 1;*/ }
    snprintf(portstr, sizeof(portstr), "%d", port); // for getaddrinfo()
    return 0;
  }

  int read_headers (sock_t *sk) {
    int hdr_size = 0;
    // now read HTTP headers
    clbuf->used = 0;
    for (;;) {
      int found, sz, rd;
      // read data from client or timeout in TIMEOUT_CLIENT msecs
      if (clbuf->used >= MAX_HDR_SIZE) {
        ptlogf("WARNING: headers too big in worker thread!\n");
        return -1;
      }
      //
      if (!sock_is_ssl(sk)) {
        // normal reader
        rd = sock_recv(sk, clbuf->buf+clbuf->used, MAX_HDR_SIZE-clbuf->used, MSG_PEEK);
        if (rd <= 0) {
          ptlogf("WARNING: headers recv error in worker thread (connection closed;fd=%d)!\n", sock_fd(sk));
          return -1;
        }
        sz = clbuf->used+rd;
        found = 0;
        // check if we get complete headers
        for (hdr_size = 0; hdr_size < sz; ++hdr_size) {
          if (clbuf->buf[hdr_size] == '\n') {
            if (hdr_size+1 < sz && clbuf->buf[hdr_size+1] == '\n') {
              found = 1;
              hdr_size += 2;
              break;
            }
            if (hdr_size+2 < sz && clbuf->buf[hdr_size+1] == '\r' && clbuf->buf[hdr_size+2] == '\n') {
              found = 1;
              hdr_size += 3;
              break;
            }
          } else if ((unsigned char)(clbuf->buf[hdr_size]) < 32 && clbuf->buf[hdr_size] != '\r' && clbuf->buf[hdr_size] != '\n' && clbuf->buf[hdr_size] != '\t') {
            ptlogf("WARNING: invalid char in headers in worker thread!\n");
            return -1;
          }
        }
        if (!found && rd == 0) {
          ptlogf("WARNING: connection closed before headers received in worker thread!\n");
          return -1;
        }
        if (found) {
          if (hdr_size <= clbuf->used) {
            // the thing that should not be
            ptlogf("FATAL: header reader: the thing that should not be!\n");
            abort();
          }
          clbuf->used += recv(sock_fd(sk), clbuf->buf+clbuf->used, hdr_size-clbuf->used, 0); // this will always succeed
          break;
        }
        clbuf->used += recv(sock_fd(sk), clbuf->buf+clbuf->used, rd, 0); // this will always succeed
      } else {
        // SSL reader
        rd = sock_recv(sk, clbuf->buf+clbuf->used, 1, MSG_WAITALL);
        if (rd != 1) {
          ptlogf("WARNING: SSL header reader failed!\n");
          return -1;
        }
        ++clbuf->used;
        int lpos = clbuf->used-1;
        if (clbuf->buf[lpos] == '\n') {
          --lpos; // remove LF
          if (lpos >= 0 && clbuf->buf[lpos] == '\r') --lpos; // remove CR
          if (lpos >= 0 && clbuf->buf[lpos] == '\n') {
            hdr_size = clbuf->used;
            break;
          }
        }
      }
    }
    // check headers size
    if (hdr_size < 10) {
      ptlogf("WARNING: headers too small in worker thread!\n");
      return -1;
    }
    clbuf->buf[hdr_size] = 0;
    ptlogf("read_headers(%d): %d bytes read\n", sock_fd(sk), hdr_size);
    // remove last '\r\n'
    if (clbuf->buf[hdr_size-1] != '\n') {
      ptlogf("WARNING: invalid headers in worker thread!\n");
      return -1;
    }
    if (clbuf->buf[hdr_size-2] == '\r') clbuf->buf[hdr_size-2] = 0; else clbuf->buf[hdr_size-1] = 0;
    // check content-length
    {
      char *cl = buf_find_header(clbuf->buf, "Content-Length:");
      content_length = -1;
      if (cl != NULL) {
        if (*cl >= '0' && *cl <= '9') {
          content_length = 0;
          while (*cl && (unsigned char)(*cl) > 32) {
            if (*cl >= '0' && *cl <= '9') {
              content_length = content_length*10+cl[0]-'0';
              ++cl;
            } else {
              content_length = -1;
              break;
            }
          }
          while (*cl && (unsigned char)(*cl) <= 32 && *cl != '\r' && *cl != '\n') ++cl;
          if (*cl != '\r' && *cl != '\n') content_length = -1;
        }
        if (content_length < 0) buf_remove_header(clbuf->buf, "Content-Length:");
      }
    }
    //
    return 0;
  }

  int do_xbuf_rd (sock_t *sk, xbuf_t *buf, const char *who) {
    if (buf->used < buf->size) {
      // try to read in buffer
      //ptlogf("do_xbuf_rd: %s reading...\n", who);
      int xr = xbuf_read(sk, buf);
      //ptlogf("do_xbuf_rd: %s read; xr=%d; used=%d\n", who, xr, buf->used);
      if (xr <= 0) {
        ptlogf("tunneling: %s connection closed (2) (0)\n", who);
        return 0;
      }
    } else {
      // do nothing
      /*
      // just check if connection is alive
      int res = asnet_recv_check(sock_fd(sk));
      if (res <= 0) {
        ptlogf("tunneling: %s connection closed (2) (1)\n", who);
        return 0;
      }
      */
    }
    return 1;
  }

  void do_tunnel (void) {
    ptlogf("established tunnel to %s:%d\n", ipstr, port);
    clbuf->used = 0;
    svbuf->used = 0;
    while (sock_is_alive(&srvfd) && sock_is_alive(&clifd)) {
      int st_r, cl_r;
      //
      coro_fd_add(sock_fd(&srvfd), SAR_READ|(clbuf->used > 0 ? SAR_WRITE : 0), TIMEOUT_SERVER);
      coro_fd_add(sock_fd(&clifd), SAR_READ|(svbuf->used > 0 ? SAR_WRITE : 0), TIMEOUT_SERVER);
      // wait
      coro_yield();
      // something was hit here, cancel all waiters
      st_r = coro_fd_check(sock_fd(&srvfd));
      cl_r = coro_fd_check(sock_fd(&clifd));
      //ptlogf("TUNNELING: st_r=0x%02x; cl_r=0x%02x\n", st_r, cl_r);
      if ((st_r|cl_r)&SAR_TIMEOUT) {
        // either client or server times out
        if (st_r&SAR_TIMEOUT) {
          ptlogf("tunneling: server connection timeouted (0)\n");
          sock_abort(&srvfd);
        }
        if (cl_r&SAR_TIMEOUT) {
          ptlogf("tunneling: client connection timeouted (0)\n");
          sock_abort(&clifd);
        }
        break;
      }
      //
      if (st_r&SAR_READ) {
        // can read from server
        if (do_xbuf_rd(&srvfd, svbuf, "server") == 0) {
          sock_abort(&srvfd);
        }
      }
      //
      if (cl_r&SAR_READ) {
        // can read from client
        if (do_xbuf_rd(&clifd, clbuf, "client") == 0) {
          sock_abort(&clifd);
        }
      }
      //
      if (sock_is_alive(&srvfd) && clbuf->used > 0 && (st_r&SAR_WRITE)) {
        // can send data to server
        //ptlogf("tunneling: writing to server (%d)...\n", clbuf->used);
        int xr = xbuf_write(&srvfd, clbuf);
        if (xr <= 0) {
          ptlogf("tunneling: server connection closed (1)\n");
          sock_abort(&srvfd);
          break;
        }
      }
      //
      if (sock_is_alive(&clifd) && svbuf->used > 0 && (cl_r&SAR_WRITE)) {
        // can send data to client
        //ptlogf("tunneling: writing to client (%d)...\n", svbuf->used);
        int xr = xbuf_write(&clifd, svbuf);
        if (xr <= 0) {
          ptlogf("tunneling: client connection closed (1)\n");
          sock_abort(&clifd);
          break;
        }
      }
    }
    // now we have one or zero fds opened
    if (sock_is_alive(&srvfd) && sock_is_alive(&clifd)) {
      ptlogf("WTF?!!\n");
      goto tunnel_error;
    }
    // send client data to server
    while (sock_is_alive(&srvfd) && clbuf->used > 0) {
      //ptlogf("tunnel: sending server data; fd=%d; left=%d\n", srvfd, clbuf->used);
      coro_fd_add(sock_fd(&srvfd), SAR_WRITE, TIMEOUT_SERVER);
      coro_yield();
      //ptlogf("tunnel: sending server data; fd=%d; st=0x%02x\n", srvfd, coro_fd_check(srvfd));
      if (coro_fd_check(sock_fd(&srvfd))&SAR_WRITE) {
        int xr = xbuf_write(&srvfd, clbuf);
        if (xr <= 0) {
          //ptlogf("tunnel: sending server data: error (0)\n");
          break;
        }
      } else {
        //ptlogf("tunnel: sending server data: error (1)\n");
        break;
      }
    }
    // send server data to client
    while (sock_is_alive(&clifd) && svbuf->used > 0) {
      //ptlogf("tunnel: sending client data; fd=%d; left=%d\n", clifd, svbuf->used);
      coro_fd_add(sock_fd(&clifd), SAR_WRITE, TIMEOUT_CLIENT);
      coro_yield();
      //ptlogf("tunnel: sending client data; fd=%d; st=0x%02x\n", clifd, coro_fd_check(clifd));
      if (coro_fd_check(sock_fd(&clifd))&SAR_WRITE) {
        int xr = xbuf_write(&clifd, svbuf);
        if (xr <= 0) {
          //ptlogf("tunnel: sending client data: error (0)\n");
          break;
        }
      } else {
        //ptlogf("tunnel: sending client data: error (1)\n");
        break;
      }
    }
tunnel_error:
    ptlogf("tunnel: server sent %llu bytes, client sent %llu bytes\n", svbuf->total, clbuf->total);
  }

  //////////////////////////////////////////////////////////////////////////////
  int build_etag (char *dest, const char *fname) {
    struct stat st;
    if (stat(fname, &st) != 0) return -1;
    if (!S_ISREG(st.st_mode)) return -1;
    sprintf(dest, "\"e%08x%08x%08x%08x\"", (unsigned int)st.st_dev, (unsigned int)st.st_ino, (unsigned int)st.st_mtime, (unsigned int)st.st_size);
    return 0;
  }

  void send_file (const char *fname, const char *ftag) {
    static char etag[512]; // this is safe, we have only one thread
    char *filedata = NULL;
    int fd;
    off_t filesize;
    if (build_etag(etag, fname) != 0) {
xnotfound:
      //ptlogf("send_file: '%s' not found!\n", fname);
      send_full(&clifd, "HTTP/1.0 404 SHIT!\r\nConnection: close\r\n\r\n", -1);
      goto xquit;
    }
    if (ftag != NULL && strcmp(ftag, etag) == 0) {
      // cache hit!
      ptlogf("CACHE HIT FOR [%s]: %s\n", fname, ftag);
      send_full(&clifd, "HTTP/1.0 304 ALWAYS\r\nConnection: close\r\n\r\n", -1);
      goto xquit;
    } else {
      ptlogf("CACHE MISS FOR [%s]: %s is not %s\n", fname, (ftag != NULL ? ftag : "\"\""), etag);
    }
    if ((fd = open(fname, O_RDONLY)) < 0) goto xnotfound;
    filesize = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);
    filedata = malloc(filesize+1);
    if (filesize > 0) read(fd, filedata, filesize); //FIXME: check for errors
    close(fd);
    sprintf(clbuf->buf, "HTTP/1.0 200 HERE\r\nConnection: close\r\nETag: %s\r\nContent-Type: %s\r\nContent-Size: %d\r\n\r\n", etag, get_content_type(fname), (int)filesize);
    if (send_full(&clifd, clbuf->buf, -1) < 0) {
      goto xquit;
    }
    if (filesize > 0) send_full(&clifd, filedata, filesize);
xquit:
    if (filedata != NULL) free(filedata);
  }

  // note that fname can point to clbuf->buf here
  void receive_file (const char *fname) {
    if (fname != NULL && fname[0] && fname[strlen(fname)-1] != '/' && content_length >= 0) {
      static char nbuf[1024];
      int fd;
      snprintf(nbuf, sizeof(nbuf), "%s.%d", fname, sock_fd(&clifd));
      ptlogf("receiving %d bytes to '%s'\n", (int)content_length, nbuf);
      fd = open(nbuf, O_WRONLY|O_CREAT|O_TRUNC, 0644);
      if (fd >= 0) {
        while (content_length > 0) {
          int toread = (content_length > svbuf->size ? svbuf->size : (int)content_length);
          int rd = sock_recv(&clifd, svbuf->buf, toread, MSG_WAITALL);
          if (rd <= 0) break;
          write(fd, svbuf->buf, rd);
          content_length -= rd;
        }
        close(fd);
        // 'rename' is atomic operation
        if (rename(nbuf, fname) < 0) {
          ptlogf("\2CAN'T RENAME '%s' to '%s'!\n", nbuf, fname);
          unlink(nbuf);
        }
        send_full(&clifd, "HTTP/1.0 200 OK\r\nConnection: close\r\n\r\n", -1);
        return;
      }
    }
    send_full(&clifd, "HTTP/1.0 500 SHIT\r\nConnection: close\r\n\r\n", -1);
  }

  //////////////////////////////////////////////////////////////////////////////
  int do_ssl_mitm_headers (void) {
    int res;

    fake_cert = malloc(sizeof(*fake_cert));
    if (fake_cert == NULL) abort(); // the thing that should not be

    ptlogf("CONNECT, loading root certificate...\n");
    cert_load_root(fake_cert);
    if (cert_for_host(hostname, fake_cert, skip_host_blocks) < 0) return -1;

    if (fake_cert->next == NULL) {
      ptlogf("SSL: we DON'T have two certificates!\n");
      return -1;
    }

    if (send_full(&clifd, "HTTP/1.0 200 OK\r\nConnection: close\r\nProxy-Connection: close\r\n\r\n", -1) != 0) return -1;

    clifd.ssl = malloc(sizeof(clifd.ssl[0]));
    if (clifd.ssl == NULL) abort(); // the thing that should not be
    if ((res = ssl_init(clifd.ssl)) != 0) {
      ptlogf("FUCKED: ssl_init returned %d\n\n", res);
      return -1;
    }

    //ssl_set_dbg(clifd.ssl, ssl_debug, stderr);
    ssl_set_endpoint(clifd.ssl, SSL_IS_SERVER);
    ssl_set_authmode(clifd.ssl, SSL_VERIFY_NONE);
    ssl_set_rng(clifd.ssl, ctr_drbg_random, &ssl_ctr_drbg_srv);
    ssl_set_bio(clifd.ssl, xssl_net_recv, &clifd, xssl_net_send, &clifd);
    ssl_set_ca_chain(clifd.ssl, fake_cert, NULL, NULL);
    ssl_set_own_cert(clifd.ssl, fake_cert->next, &ssl_pkey);

    srvfd.ssl = malloc(sizeof(srvfd.ssl[0]));
    if (srvfd.ssl == NULL) abort();
    if (ssl_init(srvfd.ssl) != 0) return -1;

    //ssl_set_dbg(srvfd.ssl, ssl_debug, stderr);
    ssl_set_endpoint(srvfd.ssl, SSL_IS_CLIENT);
    ssl_set_authmode(srvfd.ssl, SSL_VERIFY_NONE);
    ssl_set_rng(srvfd.ssl, ctr_drbg_random, &ssl_ctr_drbg_cli);
    ssl_set_bio(srvfd.ssl, xssl_net_recv, &srvfd, xssl_net_send, &srvfd);

    sock_handshake(&clifd);

    ptlogf("SSL: reading headers from client...\n");
    if (read_headers(&clifd) < 0) return -1;
    if (opt_dump_client_headers) ptlogf("===\n%s===\n", clbuf->buf);

    if (strncasecmp(clbuf->buf, "GET ", 4) == 0) mt = MT_GET;
    else if (strncasecmp(clbuf->buf, "POST ", 5) == 0) mt = MT_POST;
    else if (strncasecmp(clbuf->buf, "HEAD ", 5) == 0) mt = MT_HEAD;
    else if (strncasecmp(clbuf->buf, "OPTIONS ", 8) == 0) mt = MT_OPTIONS;
    else {
      ptlogf("WARNING: invalid method in headers in worker thread!\n");
      return -1;
    }

    cport = port;
    strcpy(chostname, hostname);
    if (fix_query() < 0) {
      ptlogf("WARNING: can't parse client query in headers in worker thread!\n===\n%s===\n", clbuf->buf);
      return -1;
    }

    return 0;
  }

  //////////////////////////////////////////////////////////////////////////////
  int do_fuck_youtube (void) {
    fake_cert = malloc(sizeof(*fake_cert));
    if (fake_cert == NULL) abort(); // the thing that should not be

    ptlogf("CONNECT, loading root certificate...\n");
    cert_load_root(fake_cert);
    if (cert_for_host(hostname, fake_cert, skip_host_blocks) < 0) return -1;

    if (fake_cert->next == NULL) {
      ptlogf("SSL: we DON'T have two certificates!\n");
      return -1;
    }

    srvfd.ssl = malloc(sizeof(srvfd.ssl[0]));
    if (srvfd.ssl == NULL) abort();
    if (ssl_init(srvfd.ssl) != 0) return -1;

    //ssl_set_dbg(srvfd.ssl, ssl_debug, stderr);
    ssl_set_endpoint(srvfd.ssl, SSL_IS_CLIENT);
    ssl_set_authmode(srvfd.ssl, SSL_VERIFY_NONE);
    ssl_set_rng(srvfd.ssl, ctr_drbg_random, &ssl_ctr_drbg_cli);
    ssl_set_bio(srvfd.ssl, xssl_net_recv, &srvfd, xssl_net_send, &srvfd);

    return 0;
  }

  //////////////////////////////////////////////////////////////////////////////
  //Content-Type: text/x-dsrc; name="bug.d"; charset=
  int compareContentType (const char *str, const char *pat) {
    if (pat == NULL || pat[0] == 0) return 0;
    if (str == NULL || str[0] == 0) return 0;
    size_t slen = strlen(str);
    size_t plen = strlen(pat);
    if (plen > slen && str[plen] != ';') return 0; // alas
    return (strncasecmp(str, pat, plen) == 0);
  }

  //////////////////////////////////////////////////////////////////////////////
  sock_init_with_fd(&clifd, me->clifd, 1); // client socket
  sock_init_empty(&srvfd, 0); // server socket
  //
  ptlogf("*** new connection! (workers=%d; fd=%d)\n", coro_count, sock_fd(&clifd));
  // disable Nagle
  {
    int state = 1;
    setsockopt(sock_fd(&clifd), IPPROTO_TCP, TCP_NODELAY, &state, sizeof(state));
  }
  //
  chostname[0] = 0;
  //
  if (read_headers(&clifd) < 0) goto quit;
  //socket_set_timeouts(sock_fd(&clifd), TIMEOUT_CLIENT);
  //
  if (opt_dump_client_headers) ptlogf("===\n%s===\n", clbuf->buf);
  // determine method
  if (strncasecmp(clbuf->buf, "GET ", 4) == 0) mt = MT_GET;
  else if (strncasecmp(clbuf->buf, "POST ", 5) == 0) mt = MT_POST;
  else if (strncasecmp(clbuf->buf, "HEAD ", 5) == 0) mt = MT_HEAD;
  else if (strncasecmp(clbuf->buf, "OPTIONS ", 8) == 0) mt = MT_OPTIONS;
  else if (strncasecmp(clbuf->buf, "CONNECT ", 8) == 0) mt = MT_CONNECT;
  else {
    ptlogf("WARNING: invalid method in headers in worker thread!\n");
    goto quit;
  }
  //
  // check and fix query, add 'Host:' if necessary
  if (fix_query() < 0) {
    ptlogf("WARNING: can't parse client query in headers in worker thread!\n===\n%s===\n", clbuf->buf);
    goto quit;
  }
  /*
  ptlogf("SLEEPING STARTED...");
  coro_sleep(2500);
  ptlogf("SLEEPING FINISHED...");
  */
  // use https for .googlevideo.com
  if (mt != MT_CONNECT) {
    const char *p = strstr(hostname, ".googlevideo.com");
    if (p != NULL && strcmp(p, ".googlevideo.com") == 0) {
      ptlogf("fucking youtube...");
      fuckYoutube = 4;
      while (buf_remove_header(clbuf->buf, "Host:") > 0) ;
      strcat(clbuf->buf, "Host: ");
      strcat(clbuf->buf, hostname);
      strcat(clbuf->buf, ":443\r\n");
    }
  }
  //
  defport = ((mt == MT_CONNECT && port == 443) || (mt != MT_CONNECT && port == 80));
  //
  if (mt == MT_CONNECT) {
    // SSL MITM: read SSL headers
    if (strncmp(hostname, "fuck.", 5) == 0) {
      // remove "fuck."
      skip_host_blocks = 1;
      memmove(hostname, hostname+5, strlen(hostname+5)+1);
    }
    //
    if (!skip_host_blocks && is_forbidden_host(mt, hostname, port)) {
      ptlogf("\x02WARNING: host blocked: [%s]!\n", hostname);
      send_full(&clifd, "HTTP/1.0 403 SSL-Fucked\r\nContent-Length: 0\r\nConnection: close\r\nProxy-Connection: close\r\n\r\n", -1);
      goto quit;
    }
    if (do_ssl_mitm_headers() < 0) goto quit;
  } else if (fuckYoutube) {
    if (do_fuck_youtube() < 0) goto quit;
  }
  //
  if (is_v4_host(mt, hostname, port)) allow_v6 = 0;
  else if (is_v6_host(mt, hostname, port)) allow_v4 = 0;
  //
  {
    const repattern_t *ua_rep;
    char och;
    char *path_start, *path_end;
    path_start = clbuf->buf;
    while ((unsigned char)(*path_start) > 32) ++path_start;
    while ((unsigned char)(*path_start) <= 32) ++path_start;
    if (mt != MT_CONNECT) {
      for (path_end = path_start; (unsigned char)(*path_end) > 32; ++path_end) ;
    } else {
      path_end = path_start;
    }
    och = *path_end;
    *path_end = 0;
    //
    if (skip_host_blocks) {
      // add back "fuck."
      memmove(hostname+5, hostname, strlen(hostname)+1);
      strcpy(hostname, "fuck");
      hostname[4] = '.';
      //ptlogf("\x01FUCKAGIN URI: [%s://%s:%d%s]!\n", proto, hostname, port, path_start);
    }
    //ptlogf("\x01FUCKAGIN URI: [%s://%s:%d%s]!\n", proto, hostname, port, path_start);
    // check for svf/swf and fuck 'em
    if (!skip_host_blocks) {
      char *qq = strchr(path_start, '?');
      if (qq == NULL) qq = path_start+strlen(path_start);
      char *qh = strchr(path_start, '#');
      if (qh == NULL) qh = path_start+strlen(path_start);
      if (qh < qq) qq = qh;
      char oldch = *qq;
      *qq = 0;
      char *ext = strrchr(path_start, '.');
      if (ext != NULL) {
        if (strcasecmp(ext, ".svg") == 0 ||
            strcasecmp(ext, ".swf") == 0
           )
        {
          ptlogf("\x02WARNING: SVG/SWF blocked: [%s://%s:%d%s]!\n", proto, hostname, port, path_start);
          goto send_biohazard;
        }
        isRZX = (strcasecmp(ext, ".rzx") == 0);
      }
      *qq = oldch;
    }
    //
    if (!skip_host_blocks && is_blocked_uri(mt, proto, hostname, port, path_start)) {
      ptlogf("\x02WARNING: URI blocked: [%s://%s:%d%s]!\n", proto, hostname, port, path_start);
      send_full(&clifd, "HTTP/1.0 403 Fucked\r\nContent-Length: 0\r\nConnection: close\r\nProxy-Connection: close\r\n\r\n", -1);
      goto quit;
    }
    // rewrite URI
    //int wasRewrite = 0;
    for (int loops = 127; loops > 0; --loops) {
      int use_loc = 0;
      int do_again = 0;
      rewritten_uri = rewrite_list_find(&re_rewrites, proto, hostname, port, path_start, &use_loc, &do_again);
      if (rewritten_uri != NULL) {
        cracked_url_t url;
        //wasRewrite = 1;
        if (use_loc) {
          char *ss = malloc(2048);
          ptlogf("\4REWRITE-LOC: [%s://%s:%d%s] ==> [%s]\n", proto, hostname, port, path_start, rewritten_uri);
          sprintf(ss, "HTTP/1.0 307 Temporary\r\nLocation: %s\r\n\r\n", rewritten_uri);
          send_full(&clifd, ss, -1);
          free(ss);
          goto quit;
        }
        ptlogf("\4REWRITE: [%s://%s:%d%s] ==> [%s]\n", proto, hostname, port, path_start, rewritten_uri);
        if (crack_url(&url, rewritten_uri) == 0) {
          int olen, nlen;
          char *np;
          snprintf(proto, sizeof(proto), "%s", url.scheme);
          snprintf(hostname, sizeof(hostname), "%s", url.host);
          port = url.port;
          /* build new path string */
          olen = (int)(path_end-path_start);
          if (url.path == NULL || !url.path[0]) {
            nlen = 1;
            np = strdup("/");
          } else {
            nlen = strlen(url.path);
            if (url.query != NULL && url.query[0]) nlen += strlen(url.query);
            if (url.fragment != NULL && url.fragment[0]) nlen += strlen(url.fragment);
            np = calloc(1, nlen+1);
            strcat(np, url.path);
            if (url.query != NULL && url.query[0]) strcat(np, url.query);
            if (url.fragment != NULL && url.fragment[0]) strcat(np, url.fragment);
          }
          cracked_url_clear(&url);
          /* fix query */
          *path_end = och; /* restore old char */
          //ptlogf("\6OLD HEADERS:\n%s", clbuf->buf);
          //ptlogf("\6PEND:\n%s", path_end);
          //ptlogf("olen=%d; nlen=%d; np=[%s]", olen, nlen, np);
          if (olen < nlen) {
            /* need more space */
            int sub = nlen-olen;
            memmove(path_end+sub, path_end, strlen(path_end)+1);
            path_end += sub;
          } else if (olen > nlen) {
            /* too much space */
            int add = olen-nlen;
            memmove(path_end-add, path_end, strlen(path_end)+1);
            path_end -= add;
          }
          //ptlogf("\5INTERIM HEADERS:\n%s", clbuf->buf);
          memcpy(path_start, np, strlen(np));
          *path_end = ' ';
          //ptlogf("\7INTERIM HEADERS:\n%s", clbuf->buf);
          free(np);
          /* fix 'host' field */
          while (buf_remove_header(clbuf->buf, "Host:") > 0) ;
          strcat(clbuf->buf, "Host: ");
          strcat(clbuf->buf, hostname);
          strcat(clbuf->buf, "\r\n");
          //ptlogf("\6FIXED HEADERS:\n%s", clbuf->buf);
          /* trunk headers again */
          och = *path_end;
          *path_end = 0;
          if (do_again) continue;
        }
      }
      break;
    }
    // rewrite complete
    if (/*skip_host_blocks && !wasRewrite &&*/ strncmp(hostname, "fuck.", 5) == 0) {
      // remove "fuck."
      skip_host_blocks = 1;
      memmove(hostname, hostname+5, strlen(hostname+5)+1);
    }
    //
    if (!skip_host_blocks && is_forbidden_host(mt, hostname, port)) {
      ptlogf("\x02WARNING: host blocked: [%s]!\n", hostname);
      send_full(&clifd, "HTTP/1.0 403 Fucked\r\nContent-Length: 0\r\nConnection: close\r\nProxy-Connection: close\r\n\r\n", -1);
      goto quit;
    }
    //
    if (mt != MT_CONNECT && !sock_is_ssl(&clifd)) {
      const char *newhost = is_https_host(mt, hostname, port);
      if (newhost != NULL) {
        char *ss = malloc(2048);
        ptlogf("\x01http-->https: [%s] ==> [%s]\n", hostname, newhost);
        sprintf(ss, "HTTP/1.0 307 Temporary\r\nLocation: https://%s%s\r\n\r\n", newhost, path_start);
        //ptlogf("<%s>\n", ss);
        send_full(&clifd, ss, -1);
        free(ss);
        goto quit;
      }
    }
    //
    if ((allow_iframe = is_iframe_uri(mt, proto, hostname, port, path_start)) != 0) {
      /*ptlogf("WARNING: FRAMES URI: [%s://%s:%d%s]!\n", proto, hostname, port, path_start);*/
    }
    //
    if ((ua_rep = relist_find(&re_uareplace, mt, proto, hostname, port, path_start)) != NULL) {
      //while (buf_remove_header(clbuf->buf, "User-Agent:") > 0) ;
      //strcat(clbuf->buf, "User-Agent: Opera/9.80\r\n");
      new_ua = ua_rep->repl;
      //ptlogf("NEW UA: %s\n", new_ua);
    } else {
      new_ua = "Mozilla/5.0 (Linux; en-US; rv:22.0) Gecko/20130405 Firefox/22.0";
    }
    //
    snprintf(pathbuf, sizeof(pathbuf), "%s", path_start);
    *path_end = och;
    //
    // remove cookies if "K8-Kill-Cookies" header is present (for xmlhttprequest)
    int killCookies = 0;
    while (buf_remove_header(clbuf->buf, "K8-Kill-Cookies:") > 0) killCookies = 1;
    if (killCookies) {
      while (buf_remove_header(clbuf->buf, "Cookie:") > 0) ;
      while (buf_remove_header(clbuf->buf, "Cookie2:") > 0) ;
    }
  }
  //ptlogf("host: [%s]; port=%d\n", hostname, port);
  // remove unneded headers
  //while (buf_remove_header(clbuf->buf, "Proxy-Connection:") > 0) ;
  while (buf_remove_header(clbuf->buf, "Connection:") > 0) ;
  while (buf_remove_header(clbuf->buf, "Keep-Alive:") > 0) ;
  while (buf_remove_header(clbuf->buf, "Proxy-") > 0) ;
  //
  while (buf_remove_header(clbuf->buf, "Accept:") > 0) ;
  while (buf_remove_header(clbuf->buf, "Accept-Language:") > 0) ;
  while (buf_remove_header(clbuf->buf, "Accept-Encoding:") > 0) ;
  //
  if (mt == MT_POST && content_length < 0) {
    ptlogf("WARNING: no 'Content-Length' in POST in worker thread!\n");
    goto quit;
  }
  //
  while (buf_remove_header(clbuf->buf, "User-Agent:") > 0) ;
  if (new_ua != NULL) {
    strcat(clbuf->buf, "User-Agent: ");
    strcat(clbuf->buf, new_ua);
    strcat(clbuf->buf, "\r\n");
  }
  //
  if (!is_referer_allowed(mt, hostname, port)) {
    while (buf_remove_header(clbuf->buf, "Referer:") > 0) ;
  } else {
    // fix referer: rewrite referer if it points to foreign host
    int do_remove = 0;
    char *ref = buf_find_header(clbuf->buf, "Referer:");
    if (ref != NULL) {
      char *end = strpbrk(ref, "\r\n");
      if (end != NULL && end > ref) {
        static char refstr[4096];
        memcpy(refstr, ref, end-ref);
        refstr[end-ref] = 0;
        ref = strstr(refstr, "://");
        if (ref != NULL) {
          ref += 3;
          end = strchr(ref, '/');
          if (end == NULL) end = ref+strlen(ref);
          *end = 0;
          if (strcasecmp(ref, hostname) != 0) {
            ptlogf("WARNING: rewriting invalid referer!\n");
            while (buf_remove_header(clbuf->buf, "Referer:") > 0) ;
            sprintf(clbuf->buf+strlen(clbuf->buf), "Referer: %s://%s:%d%s\r\n", proto, hostname, port, pathbuf);
            ptlogf("NEW REFERER: %s://%s:%d%s\r\n", proto, hostname, port, pathbuf);
          }
        } else {
          do_remove = 1;
        }
      } else {
        do_remove = 1;
      }
      if (do_remove) while (buf_remove_header(clbuf->buf, "Referer:") > 0) ;
    }
  }
  if (mt != MT_CONNECT) {
    strcat(clbuf->buf, "Connection: close\r\n");
    //strcat(clbuf->buf, "\r\n");
    //ptlogf("host: %s\nport: %s\n---\n%s---\n", hostname, portstr, clbuf->buf);
  }
  strcat(clbuf->buf, "Accept: */*\r\n");
  //strcat(clbuf->buf, "Accept-Encoding: identity\r\n");
  strcat(clbuf->buf, "Accept-Encoding: deflate\r\n");
  strcat(clbuf->buf, "\r\n");
  {
    char *t = strchr(clbuf->buf, '\n'), *p = clbuf->buf;
    *t = 0;
    p = clbuf->buf;
    while (*p && (unsigned char)(*p) > 32) ++p;
    while (*p && (unsigned char)(*p) <= 32) ++p;
    ptlogf("\1QUERY: %s %s://%s%s%s%s\n", mt_names[mt], proto, hostname, (defport ? "" : ":"), (defport ? "" : portstr), (mt != MT_CONNECT ? p : ""));
    *t = '\n';
  }
  // connect to host
  if (strcmp(hostname, "mediator") == 0) {
    // wow, this is our control panel
    // get path
    char *path = clbuf->buf, *etag, *e;
    while (*path && (unsigned char)(*path) > 32) ++path;
    while (*path && (unsigned char)(*path) <= 32) ++path;
    for (e = path; *e && *e != '\n' && (unsigned char)(*e) > 32; ++e) ;
    if (e > path && e[-1] == '\r') --e;
    *e = 0;
    // get etag
    etag = strcasestr(e+1, "\nIf-None-Match:");
    if (etag != NULL) {
      etag += 15;
      while (*etag && *etag != '\r' && *etag != '\n' && (unsigned char)(*etag) <= 32) ++etag;
      for (e = etag; *e && *e != '\r' && *e != '\n'; ++e) ;
      *e = 0;
    }
#include "mdcc.c"
    send_full(&clifd, "HTTP/1.0 404 FUCK\r\nContent-Type: text/plain\r\n\r\nWTF?!", -1);
    goto quit;
  }
  //
  if (opt_noetag) {
    while (buf_remove_header(clbuf->buf, "ETag:") > 0) ;
    //while (buf_remove_header(clbuf->buf, "If-Modified-Since:") > 0) ;
    //while (buf_remove_header(clbuf->buf, "If-None-Match:") > 0) ;
    // fuck all conditionals, yeah
    while (buf_remove_header(clbuf->buf, "If-") > 0) ;
  }
  //
  if (opt_dump_client_headers) {
    ptlogf("===\n%s===\n", clbuf->buf);
    /*
    FILE *fo = fopen("/home/ketmar/k8prj/mediator/zx000.hdr", "a");
    fprintf(fo, "==========\n");
    fwrite(clbuf->buf, strlen(clbuf->buf), 1, fo);
    fprintf(fo, "----------\n");
    fclose(fo);
    */
  }
  //
  if (chostname[0] == 0) strcpy(chostname, hostname);
  if (cport == 0) cport = port;
  snprintf(portstr, sizeof(portstr), "%d", cport);
  //
//againyt:
  if (chostname[0] == '[' && chostname[strlen(chostname)-1] == ']') {
    //IPv6
    //memmove(chostname, chostname+1, strlen(chostname)+1);
    //chostname[strlen(chostname)-1] = 0;
    hn = chostname+1;
    hn[strlen(hn)-1] = 0;
    allow_v6 = 1;
  } else {
    hn = chostname;
  }
  //
  memset(&me->hints, 0, sizeof(me->hints));
  me->hints.ai_family = AF_UNSPEC;
  me->hints.ai_socktype = SOCK_STREAM;
  me->hints.ai_flags = 0|AI_ALL/*|AI_CANONNAME*/;
  me->hints.ai_protocol = 0;
  me->hints.ai_canonname = NULL;
  me->hints.ai_addr = NULL;
  me->hints.ai_next = NULL;
  //
  memset(&me->gacb, 0, sizeof(me->gacb));
  {
    const char *hyaddr = hypehost_find(hn);
    if (hyaddr != NULL) {
      ptlogf("Hyperborea host '%s': [%s]\n", hn, hyaddr);
      me->gacb.ar_name = hyaddr;
    } else {
      me->gacb.ar_name = hn;
    }
  }
  me->gacb.ar_service = (fuckYoutube ? "443" : portstr);
  me->gacb.ar_request = &me->hints;
  //
  me->sig.sigev_notify = SIGEV_SIGNAL;
  me->sig.sigev_signo = SIGRTMIN;
  //
  //gares = getaddrinfo(chostname, portstr, &hints, &adrres);
  me->resolving_name = 1;
  me->gacbarr = &me->gacb;
  gares = getaddrinfo_a(GAI_NOWAIT, &me->gacbarr, 1, &me->sig);
  if (gares != 0) {
    ptlogf("FATAL: getaddrinfo_a: %d\n", /*gai_strerror*/(gares));
    goto quit;
  }
  coro_yield();
  if (hn != chostname) hn[strlen(hn)] = ']'; // it's safe: it was removed earlier
  if (me->resolving_name < 0) {
    ptlogf("FATAL: can't resolve %s\n", chostname);
    goto quit;
  }
  snprintf(ipstr, sizeof(ipstr), "UNDEFINED");
  // find address and try to connect
  for (struct addrinfo *rp = me->gacb.ar_result; rp != NULL; rp = rp->ai_next) {
    //printf("family: %d (%s)\n", rp->ai_family, (rp->ai_family == AF_INET ? "IPv4" : (rp->ai_family == AF_INET6 ? "IPv6" : "unknown")));
    if (rp->ai_family == AF_INET && allow_v4) {
      struct sockaddr_in *sav4 = (void *)rp->ai_addr;
      inet_ntop(rp->ai_addr->sa_family, &sav4->sin_addr, ipstr, sizeof(ipstr));
      ptlogf("connecting to IPv4: %s:%d\n", ipstr, cport);
      //sav4->sin_port = htons(cport);
    } else if (rp->ai_family == AF_INET6 && allow_v6) {
      struct sockaddr_in6 *sav6 = (void *)rp->ai_addr;
      inet_ntop(rp->ai_addr->sa_family, &sav6->sin6_addr, ipstr+1, sizeof(ipstr)-2);
      ipstr[0] = '[';
      strcat(ipstr, "]");
      ptlogf("connecting to IPv6: %s:%d\n", ipstr, cport);
      //sav6->sin6_port = htons(cport);
      sav6->sin6_flowinfo = 0; //???
    } else {
      continue;
    }
    srvfd.fd = socket(rp->ai_family, SOCK_STREAM, 0);
    if (srvfd.fd < 0) {
      ptlogf("FATAL: can't create socket!\n");
      freeaddrinfo(me->gacb.ar_result);
      send_full(&clifd, "HTTP/1.0 500 Can't create socket!\r\nContent-Length: 0\r\nConnection: close\r\nProxy-Connection: close\r\n\r\n", -1);
      goto quit;
    }
    socket_set_nonblocking(srvfd.fd);
    //socket_set_timeouts(srvfd.fd, TIMEOUT_SERVER);
connect_again:
    if (connect(srvfd.fd, rp->ai_addr, rp->ai_addrlen) == 0) {
      ptlogf("connected to %s:%d\n", ipstr, cport);
      break;
    }
    if (errno == EINTR) goto connect_again;
    if (errno != EINPROGRESS) {
connection_failed:
      freeaddrinfo(me->gacb.ar_result);
      ptlogf("connection failed: %s:%d\n", ipstr, cport);
      sock_abort(&srvfd);
      send_full(&clifd, "HTTP/1.0 404 Can't connect to host!\r\nContent-Length: 0\r\nConnection: close\r\nProxy-Connection: close\r\n\r\n", -1);
      goto quit;
    }
    // wait for connection to complete
    {
      coro_fd_add(sock_fd(&srvfd), SAR_WRITE, TIMEOUT_SERVER);
      coro_yield();
      if (coro_fd_check(sock_fd(&srvfd)) == SAR_WRITE) {
        // check for error
        int err = 1;
        socklen_t len = sizeof(err);
        if (getsockopt(sock_fd(&srvfd), SOL_SOCKET, SO_ERROR, &err, &len) != 0 || err != 0) goto connection_failed;
      } else {
        goto connection_failed;
      }
      ptlogf("connected to %s:%d\n", ipstr, cport);
      // disable Nagle
      {
        int state = 1;
        setsockopt(sock_fd(&srvfd), IPPROTO_TCP, TCP_NODELAY, &state, sizeof(state));
      }
      break;
    }
  }
  freeaddrinfo(me->gacb.ar_result);
  //
  if (!sock_is_alive(&srvfd)) {
    // alas
    //if (mt == MT_CONNECT) send_full(&clifd, "HTTP/1.0 404 Not Found\r\n\r\n", -1);
    send_full(&clifd, "HTTP/1.0 410 Gone\r\nContent-Length: 0\r\nConnection: close\r\nProxy-Connection: close\r\n\r\n", -1);
    goto quit;
  }
  //
  if (sock_handshake(&srvfd) < 0) {
    fuckYoutube = 0;
    if (--fuckYoutube < 0) {
      send_full(&clifd, "HTTP/1.0 500 SSL fucked\r\nContent-Type: text/plain\r\n\r\nSSL fucked", -1);
      goto quit;
    }
    // this seems to be unnecessary, so it's disabled
/*
    ptlogf("YTB(%d): sleeping 10 seconds...\n", fuckYoutube);
    coro_sleep(10000);
    ptlogf("YTB(%d): again!\n", fuckYoutube);
    sock_close(&srvfd);
    if (fake_cert != NULL) {
      cert_free(fake_cert);
      free(fake_cert);
      fake_cert = NULL;
    }
    if (do_fuck_youtube() < 0) {
      send_full(&clifd, "HTTP/1.0 500 SSL fucked YT\r\nContent-Type: text/plain\r\n\r\nSSL fucked YT", -1);
      goto quit;
    }
    goto againyt;
*/
  }
  //
  if (mt == MT_CONNECT) {
    if (send_full(&clifd, "HTTP/1.0 200 OK\r\nConnection: close\r\nProxy-Connection: close\r\n\r\n", -1) == 0) do_tunnel();
    goto quit;
  }
  //
  //ptlogf("sending server headers (%d)\n", strlen(clbuf->buf));
  // send headers to server
  /*
  {
    FILE *fo = fopen("/home/ketmar/k8prj/mediator/zx000.hdr", "a");
    //fprintf(fo, "==========\n");
    fwrite(clbuf->buf, strlen(clbuf->buf), 1, fo);
    //fprintf(fo, "----------\n");
    fclose(fo);
  }
  */
  if (send_full(&srvfd, clbuf->buf, -1) < 0) {
    ptlogf("WARNING: header sending failed: %s:%d\n", ipstr, port);
    goto quit;
  }
  //
  // post? transfer data
  if (content_length > 0) {
#ifdef DUMP_DATA
    int dumpfd = open("zdump_post.log", O_WRONLY|O_CREAT|O_TRUNC, 0644);
    write(dumpfd, "--------\n", 9);
#endif
    ptlogf("sending POST data to %s:%d\n", ipstr, port);
    while (content_length > 0) {
      int toread = (content_length > clbuf->size ? clbuf->size : (int)content_length);
      int rd = sock_recv(&clifd, clbuf->buf, toread, MSG_WAITALL);
      if (rd <= 0) {
#ifdef DUMP_DATA
        close(dumpfd);
#endif
        goto quit;
      }
#ifdef DUMP_DATA
      write(dumpfd, clbuf->buf, rd);
#endif
      if (send_full(&srvfd, clbuf->buf, rd) < 0) {
#ifdef DUMP_DATA
        close(dumpfd);
#endif
        goto quit;
      }
      content_length -= rd;
    }
#ifdef DUMP_DATA
    close(dumpfd);
#endif
  }
  //shutdown(clifd.fd, SHUT_RD);
  //shutdown(srvfd.fd, SHUT_WR);
  // get and send server headers
  ptlogf("reading server headers from %s:%d\n", ipstr, port);
  if (read_headers(&srvfd) < 0) goto quit;
  if (opt_dump_server_headers) ptlogf("+++\n%s+++\n", clbuf->buf);
  // remove SVGs
  if (!skip_host_blocks) {
    //Content-Type: image/svg+xml
    char *ct = buf_find_header(clbuf->buf, "Content-Type:");
    if (ct != NULL) {
      int doReplace = 0;
      char *end = strpbrk(ct, "\r\n");
      char oc = *end;
      *end = 0;

      if (compareContentType(ct, "text/x-dsrc") || compareContentType(ct, "text/x-asm")) {
        // this is for dlang bugzilla and opera: i tired of opera trying to open attaches in external app
        *end = oc;
        while (buf_remove_header(clbuf->buf, "Content-Type:") > 0) ;
        while (buf_remove_header(clbuf->buf, "Content-disposition:") > 0) ;
        strcat(clbuf->buf, "Content-Type: text/plain\r\n");
      } else {
             if (strcmp(ct, "image/svg+xml") == 0) doReplace = 1;
        else if (strcmp(ct, "application/x-shockwave-flash") == 0) doReplace = 1;
        else if (strcmp(ct, "application/shockwave-flash") == 0) doReplace = 1;
        if (doReplace) {
          // got it, replace with another one
send_biohazard:
          ptlogf("\x02WARNING: SVG REPLACEMENT!");
          //if (send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: image/png\r\n\r\n", -1) < 0) goto quit;
          //send_full(&clifd, hazardPNG, sizeof(hazardPNG));
          send_full(&clifd, hazardSVG, -1);
          goto quit;
        }
        *end = oc;
      }
    }
  }
  if (isRZX) {
    while (buf_remove_header(clbuf->buf, "Content-Type:") > 0) ;
    strcat(clbuf->buf, "Content-Type: application/rzx\r\n");
  }
  while (buf_remove_header(clbuf->buf, "Connection:") > 0) ;
  while (buf_remove_header(clbuf->buf, "Keep-Alive:") > 0) ;
  strcat(clbuf->buf, "Connection: close\r\n");
  strcat(clbuf->buf, "Proxy-Connection: close\r\n");
  //
  if (!allow_iframe) {
    strcat(clbuf->buf, "X-Frame-Options: DENY\r\n");
    //ptlogf("\1HOST: [%s]\n", chostname);
  } else {
    while (buf_remove_header(clbuf->buf, "X-Frame-Options:") > 0) ;
  }
  // other shit
    while (buf_remove_header(clbuf->buf, "X-Content-Type-Options:") > 0) ;
    while (buf_remove_header(clbuf->buf, "X-XSS-Protection:") > 0) ;
    while (buf_remove_header(clbuf->buf, "Alternate-Protocol:") > 0) ;
  //
  strcat(clbuf->buf, "X-DNSPrefetch-Control: off\r\n");
  strcat(clbuf->buf, "Vary: Accept-Encoding\r\n");
  strcat(clbuf->buf, "\r\n");
  if (fuckYoutube) {
    // change Location from https to http (we'll fuck it later)
    char *lc = strcasestr(clbuf->buf, "\r\nLocation:");
    if (lc != NULL) {
      lc += 11; // skip field name
      while (*lc && (*lc == '\t' || *lc == ' ')) ++lc; // skip spaces
      if (strncmp(lc, "https:", 6) == 0) {
        // yes, this is https redirection, remove 's'
        lc += 4; // skip 'http'
        memmove(lc, lc+1, strlen(lc));
        ptlogf("YT: location fixed");
      }
    }
  }
  if (opt_dump_server_headers) ptlogf("+++\n%s+++\n", clbuf->buf);
  ptlogf("sending server headers from %s:%d\n", ipstr, port);
  if (send_full(&clifd, clbuf->buf, -1) < 0) {
    ptlogf("WARNING: header sending failed\n");
    goto quit;
  }
  // now just tunnel
  if (content_length >= 0) {
#ifdef DUMP_DATA
    int dumpfd = open("zdump_reply.log", O_WRONLY|O_CREAT|O_TRUNC, 0644);
    write(dumpfd, "--------\n", 9);
#endif
    uint64_t stt, total = 0;
    ptlogf("transferring %llu bytes from %s:%d\n", content_length, ipstr, port);
    stt = k8clock();
    while (content_length > 0) {
      int toread = (content_length > clbuf->size ? clbuf->size : (int)content_length);
      int rd = sock_recv(&srvfd, clbuf->buf, toread, MSG_WAITALL);
      if (rd <= 0) {
        ptlogf("WARNING: error %d while reading from %s:%d\n", rd, ipstr, port);
#ifdef DUMP_DATA
        close(dumpfd);
#endif
        goto quit;
      }
#ifdef DUMP_DATA
      write(dumpfd, clbuf->buf, rd);
#endif
      if (send_full(&clifd, clbuf->buf, rd) < 0) {
        ptlogf("WARNING: error while sending from %s:%d\n", ipstr, port);
#ifdef DUMP_DATA
        close(dumpfd);
#endif
        goto quit;
      }
      content_length -= rd;
      total += rd;
    }
    stt = k8clock()-stt;
#ifdef DUMP_DATA
    close(dumpfd);
#endif
    if (stt >= 500) {
      ptlogf("transfer complete, average speed: %.2f KB/sec\n", ((double)total/(double)stt)*(1000.0/1024.0));
    }
  } else {
    do_tunnel();
  }
  ptlogf("session with %s:%d complete\n", ipstr, port);
quit:
  // negative fds are OK
  sock_close(&clifd);
  sock_close(&srvfd);
  if (fake_cert != NULL) {
    cert_free(fake_cert);
    free(fake_cert);
    fake_cert = NULL;
  }
  //ptlogf("*** dead connection! (workers=%d)\n", coro_count-1);
  coro_kill(me);
}
