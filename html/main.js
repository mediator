////////////////////////////////////////////////////////////////////////////////
// hack for tormozilla; not working though
if (typeof(document.selectNodes) != "function") {
  selectNodes = function (xpath) {
    var it = document.evaluate(xpath, document, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
    var res = [];
    for (var i = it.iterateNext(); i; i = it.iterateNext()) res.push(i);
    return res;
  }
} else {
  selectNodes = function (xpath) {
    return document.selectNodes(xpath);
  }
}


////////////////////////////////////////////////////////////////////////////////
function insert_after (elem, what) {
  var pn = elem.parentNode;
  if (pn) {
    if (elem.nextSibling) pn.insertBefore(what, elem.nextSibling); else pn.appendChild(what);
  }
}


////////////////////////////////////////////////////////////////////////////////
// async request
//
function HTTPRequest (obj_init) {
  this.method = "GET";
  this.timeout = 2000; // milliseconds
  if (obj_init && typeof(obj_init) == "object") {
    if (typeof(obj_init.method) == "string") this.method = obj_init.method;
    if (typeof(obj_init.url) == "string") this.url = obj_init.url;
    if (typeof(obj_init.data) == "string") this.data = obj_init.data;
    if (typeof(obj_init.user) == "string") this.user = obj_init.user;
    if (typeof(obj_init.password) == "string") this.user = obj_init.password;
    if (typeof(obj_init.headers) == "string") this.headers = [obj_init.headers];
    else if (typeof(obj_init.headers) == "object") this.headers = obj_init.headers;
    if (typeof(obj_init.on_error) == "function") this.on_error = obj_init.on_error;
    if (typeof(obj_init.on_start) == "function") this.on_start = obj_init.on_start;
    if (typeof(obj_init.on_headers) == "function") this.on_headers = obj_init.on_headers;
    if (typeof(obj_init.on_loading) == "function") this.on_loading = obj_init.on_loading;
    if (typeof(obj_init.on_done) == "function") this.on_done = obj_init.on_done;
    if (typeof(obj_init.on_cancel) == "function") this.on_cancel = obj_init.on_cancel;
  }
  return this;
}


HTTPRequest.prototype.abort = function () {
  if (this.req) {
    var cb = this.on_cancel;
    this.req.onreadystatechange = function () {};
    this.req.abort();
    this.req = null;
    if (this.timeout_id) {
      clearTimeout(this.timeout_id);
      this.timeout_id = null;
    }
    if (typeof(cb) == "function") setTimeout(function () { cb.call(this, {text:"", headers:"", status:500, status_text:"aborted"}); }, 1);
  }
}


HTTPRequest.prototype.start = function () {
  if (!this.req) {
    // not started
    var me = this; // for closure
    var req = new XMLHttpRequest();
    var err_cb = me.on_error;
    //
    var stop_timeout = function () {
      if (me.timeout_id) {
        clearTimeout(me.timeout_id);
        me.timeout_id = null;
      }
    };
    //
    var restart_timeout = function () {
      stop_timeout();
      if (me.timeout > 0) me.timeout_id = setTimeout(function () { me.abort.call(me); }, me.timeout);
    };
    //
    req.onreadystatechange = function () {
      var st = req.readyState;
      var res = {text:"", headers:"", status:0, status_text:""};
      var cb = null;
      //opera.postError("statechange: st="+st+"; status="+req.status);
      switch (st) {
        case 0: case 1: return;
        case 2: restart_timeout(); cb = me.on_headers; break;
        case 3: restart_timeout(); cb = me.on_loading; break;
        case 4:
          stop_timeout();
          res.status = req.status;
          res.status_text = (typeof(req.statusText) == "string" ? req.statusText : "???");
          if (req.status >= 200 && req.status < 300) {
            cb = me.on_done;
            //res.XML = req.responseXML;
            res.text = (typeof(req.responseText) == "string" ? req.responseText : "");
            res.headers = req.getAllResponseHeaders();
          } else {
            cb = me.on_error;
          }
          break;
        default:
          stop_timeout();
          cb = me.on_error();
          res.status = 500;
          res.status_text = "internal error: "+st;
          break;
      }
      if (typeof(cb) == "function") setTimeout(function () { cb.call(me, res); }, 1);
    };
    //
    try {
      req.open(me.method, me.url, true, me.user, me.password);
    } catch (e) {
      if (typeof(err_cb) != "function") return false;
      setTimeout(function () { err_cb.call(me, {text:"", headers:"", status:500, status_text:"internal error"}); }, 1);
      return false;
    }
    if (me.headers) {
      for (var n in me.headers) {
        if (typeof(n) == "string") req.setRequestHeader(n, me.headers[n]);
      }
    }
    try {
      req.send(typeof(me.data) == "string" ? me.data : null);
    } catch (e) {
      if (typeof(err_cb) != "function") return false;
      setTimeout(function () { err_cb.call(me, {text:"", headers:"", status:500, status_text:"internal error"}); }, 1);
      return false;
    }
    restart_timeout();
    var cb = me.on_start;
    if (typeof(cb) == "function") setTimeout(function () { cb.call(me, {text:"", headers:"", status:0, status_text:"initialized"}); }, 1);
    me.req = req;
  }
  return true;
}


////////////////////////////////////////////////////////////////////////////////
function new_element (name, attrs, inner) {
  var el = document.createElement(name);
  if (attrs) {
    for (var k in attrs) {
      var v = attrs[k];
      //opera.postError(k+"="+v);
      if (typeof(v) == "function") el.addEventListener(k, v, false);
      else el.setAttribute(k, v);
    }
  }
  if (typeof(inner) == "string") {
    el.appendChild(document.createTextNode(inner));
  } else if (inner !== null && typeof(inner) != "undefined") {
    el.appendChild(inner);
  }
  return el;
}


function set_state (id, text, color) {
  var el = document.getElementById(id);
  if (el) {
    if (text) {
      el.textContent = text;
      if (color) el.style.color = color;
      el.style.visibility = "visible";
      el.style.display = "inline";
    } else {
      el.style.visibility = "hidden";
      el.style.display = "none";
    }
  }
}


function submit_me () {
  var ta = document.getElementById("ledit");
  if (ta) {
    var path = ta.getAttribute("path");
    if (path) {
      var req = new HTTPRequest();
      req.on_error = req.on_cancel = function () { set_state("submit_span", "\u00a0ERROR!", "red"); };
      req.on_done = function (res) { set_state("submit_span", "\u00a0done", "white"); };
      req.url = path;
      req.method = "POST";
      req.data = ta.value.replace(/\r\n/g, "\n");
      set_state("submit_span", "\u00a0processing...", "yellow");
      req.start();
      return false;
    }
  }
}


function reload_me () {
  var ta = document.getElementById("ledit");
  if (ta) {
    var path = ta.getAttribute("path");
    if (path) {
      var req = new HTTPRequest();
      req.on_error = req.on_cancel = function () { set_state("reload_span", "\u00a0ERROR!", "red"); };
      req.on_done = function (res) { ta.value = res.text; set_state("reload_span", "\u00a0done", "white"); };
      req.url = path;
      set_state("reload_span", "\u00a0processing...", "yellow");
      req.start();
      return false;
    }
  }
}


function query_status (donecb) {
  var req = new HTTPRequest();
  req.on_error = req.on_cancel = function () { donecb(false); };
  req.on_done = function (res) {
    var kva = res.text.split("\n");
    var kvo = {};
    for (var f = 0; f < kva.length; ++f) {
      var kv = kva[f].trim();
      if (kv) {
        var kv = kva[f].match(/^(\S+)\s+(\S*)$/);
        //opera.postError("kv="+kv);
        if (!kv) {
          kv = kva[f].match(/^(\S+)$/);
          if (kv) kvo[kv[1]] = false;
        } else {
          kvo[kv[1]] = kv[2];
        }
      }
    }
    donecb(kvo);
  };
  req.url = "http://mediator/status";
  req.start();
}


////////////////////////////////////////////////////////////////////////////////
function setup_tasubmits () {
  var ta = document.getElementById("ledit");
  if (ta) {
    var path = ta.getAttribute("path");
    if (path) {
      var req = new HTTPRequest();
      req.on_error = req.on_cancel = function () { ta.value = "!!!ERROR!!!"; };
      req.on_done = function (res) { ta.value = res.text; };
      req.url = path;
      req.start();
      insert_after(ta, new_element("a", {"href":"/html/"}, "BACK"));
      insert_after(ta, new_element("hr"));
      insert_after(ta, new_element("span", {"id":"reload_span","class":"req_state"}));
      insert_after(ta, new_element("a", {"click":reload_me}, "reload!"));
      insert_after(ta, document.createTextNode("\u00a0|\u00a0"));
      insert_after(ta, new_element("span", {"id":"submit_span","class":"req_state"}));
      insert_after(ta, new_element("a", {"click":submit_me}, "submit!"));
      insert_after(ta, new_element("p"));
    }
  }
}


function setup_status () {
  //<div type="status" var="dump_headers_client" msg="client headers dumping"></div>
  var sl = selectNodes(".//div[@type='status']");
  if (sl.length > 0) {
    //opera.postError("sl.length="+sl.length);
    query_status(function (res) {
      if (!res) return;
      //opera.postError("res="+res);
      //opera.postError("sl.length="+sl.length);
      for (var f = 0; f < sl.length; ++f) {
        var v = res[sl[f].getAttribute("var")];
        //opera.postError("v="+v);
        if (typeof(v) == "string") {
          sl[f].innerText = sl[f].getAttribute("msg")+": "+v;
        } else {
          sl[f].innerText = sl[f].getAttribute("msg")+": UNKNOWN";
        }
      }
    });
  }
}


function setup_aspans () {
  var nl = selectNodes(".//a[@spanid != '']");
  for (var f = nl.length-1; f >= 0; --f) {
    var a = nl[f];
    var el = document.createElement("span");
    el.setAttribute("class", "req_state");
    el.setAttribute("id", a.getAttribute("spanid"));
    insert_after(a, el);
    insert_after(a, document.createTextNode("\u00a0"));
    a.addEventListener("click", function (ev) {
      var id = event.currentTarget.getAttribute("spanid");
      var req = new HTTPRequest();
      req.on_error = req.on_cancel = function () {
        set_state(id, "ERROR!", "red");
      };
      req.on_done = function () {
        set_state(id, "", "white");
        setup_status();
      };
      req.url = event.currentTarget.getAttribute("path");
      set_state(id, "processing...");
      req.start();
    }, false);
  }
}


////////////////////////////////////////////////////////////////////////////////
document.addEventListener("DOMContentLoaded", function (ev) {
  setup_tasubmits();
  setup_aspans();
  setup_status();
}, false);
