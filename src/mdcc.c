    if (strcmp(path, "/favicon.ico") == 0) {
      send_file("html/favicon.png", etag);
      goto quit;
    }

    if (strcmp(path, "/") == 0 || strcmp(path, "/html") == 0 || strcmp(path, "/html/") == 0) {
      send_full(&clifd, "HTTP/1.0 301 Permanent\r\nLocation: http://mediator/html/main.html\r\n\r\n", -1);
      goto quit;
    }

    if (strncmp(path, "/html/", 6) == 0) {
      send_file(path+1, etag);
      goto quit;
    }

    if (strncmp(path, "/data/", 6) == 0) {
      if (mt == MT_POST) {
        receive_file(path+1);
        do_hp_reload = 1;
        ptlogf("INITIATED LISTS RELOADING SEQUENCE");
      } else {
        send_file(path+1, etag);
      }
      goto quit;
    }

    if (strcmp(path, "/status") == 0 || strcmp(path, "/status/") == 0) {
      // query options
      char *m;
      int res = asprintf(&m,
        "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\n"
        "dump_headers_client %s\n"
        "dump_headers_server %s\n"
        "nolog %s\n"
        "nologfile %s\n"
        "nologtty %s\n"
        "no_etag %s\n"
        "threads %d\n"
        "",
        (opt_dump_client_headers ? "yes" : "no"),
        (opt_dump_server_headers ? "yes" : "no"),
        (!is_log_enabled() ? "yes" : "no"),
        (!is_flog_enabled() ? "yes" : "no"),
        (opt_logtty_disabled ? "yes" : "no"),
        (opt_noetag ? "yes" : "no"),
        (coro_count > 0 ? coro_count-1 : 0)); // compensate for this thread
      if (res >= 0) {
        send_full(&clifd, m, -1);
        free(m);
      }
      goto quit;
    }

    if (strcmp(path, "/shutdown") == 0) {
      if (!do_shutdown) {
        ++do_shutdown;
        ptlogf("INITIATED SHUTDOWN SEQUENCE");
      }
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nINITIATED SHUTDOWN SEQUENCE", -1);
      goto quit;
    }

    if (strcmp(path, "/reload") == 0) {
      do_hp_reload = 1;
      ptlogf("INITIATED LISTS RELOADING SEQUENCE");
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nINITIATED RELOAD SEQUENCE", -1);
      goto quit;
    }

    if (strcmp(path, "/headers/on") == 0) {
      opt_dump_client_headers = 1;
      opt_dump_server_headers = 1;
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nHEADER DUMP ON", -1);
      goto quit;
    }

    if (strcmp(path, "/headers/off") == 0) {
      opt_dump_client_headers = 0;
      opt_dump_server_headers = 0;
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nHEADER DUMP OFF", -1);
      goto quit;
    }

    if (strcmp(path, "/etag/off") == 0) {
      opt_noetag = 1;
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nETAG OFF", -1);
      goto quit;
    }

    if (strcmp(path, "/etag/on") == 0) {
      opt_noetag = 0;
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nETAG ON", -1);
      goto quit;
    }

    if (strcmp(path, "/log/on") == 0) {
      log_set_enabled(1);
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nLOGGING ON", -1);
      goto quit;
    }

    if (strcmp(path, "/log/off") == 0) {
      log_set_enabled(0);
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nLOGGING OFF", -1);
      goto quit;
    }

    if (strcmp(path, "/flog/on") == 0) {
      flog_set_enabled(1);
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nFILE LOGGING ON", -1);
      goto quit;
    }

    if (strcmp(path, "/flog/off") == 0) {
      flog_set_enabled(0);
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nFILE LOGGING OFF", -1);
      goto quit;
    }

    if (strcmp(path, "/clog/on") == 0) {
      opt_logtty_disabled = 0;
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nCONSOLE LOGGING ON", -1);
      goto quit;
    }

    if (strcmp(path, "/clog/off") == 0) {
      opt_logtty_disabled = 1;
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nCONSOLE LOGGING OFF", -1);
      goto quit;
    }

    if (strcmp(path, "/newlog") == 0) {
      log_new();
      send_full(&clifd, "HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nNEW LOG FILE STARTED", -1);
      goto quit;
    }
