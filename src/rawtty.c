/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>



static struct termios orig_tios;
static int raw_mode = 0;


int is_rawtty (void) {
  return raw_mode;
}


int rawtty_off (void) {
  if (raw_mode) {
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_tios);
    raw_mode = 0;
  }
  return 0;
}


int rawtty_on (void) {
  if (!raw_mode) {
    struct termios raw;
    if (!isatty(STDIN_FILENO)) return -1;
    if (tcgetattr(STDIN_FILENO, &orig_tios) == -1) return -1;
    raw = orig_tios; /* modify the original mode */
    /* input modes: no break, no CR to NL, no parity check, no strip char, no start/stop output control */
    /*raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);*/
    /* input modes: no break, no parity check, no strip char, no start/stop output control */
    raw.c_iflag &= ~(BRKINT | INPCK | ISTRIP | IXON);
    /* output modes - disable post processing */
    raw.c_oflag &= ~(OPOST);
    raw.c_oflag |= ONLCR;
    raw.c_oflag = OPOST | ONLCR;
    /* control modes - set 8 bit chars */
    raw.c_cflag |= (CS8);
    /* local modes - choing off, canonical off, no extended functions, no signal chars (^Z,^C) */
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    /* control chars - set return condition: min number of bytes and timer;
     * we want read to return every single byte, without timeout */
    raw.c_cc[VMIN] = 1; raw.c_cc[VTIME] = 0; /* 1 byte, no timer */
    /* put terminal in raw mode after flushing */
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) < 0) return -1;
    raw_mode = 1;
  }
  return 0;
}


int rawtty_set (int raw) {
  return (raw ? rawtty_on() : rawtty_off());
}


int rawtty_is_keyhit (void) {
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
  select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &fds);
}


/* method waitInput(ms=false) */
int rawtty_waitkey (int to_msec) {
  struct timeval tv;
  fd_set fds;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
  if (to_msec <= 0) {
    tv.tv_sec = 0;
    tv.tv_usec = 0;
  } else {
    tv.tv_sec = to_msec/1000;
    tv.tv_usec = (to_msec%1000)*1000;
  }
  select(STDIN_FILENO+1, &fds, NULL, NULL, (to_msec < 0 ? NULL : &tv));
  return (FD_ISSET(STDIN_FILENO, &fds) ? 1 : 0);
}


int rawtty_width (void) {
#ifdef TIOCGWINSZ
  struct winsize sz;
  if (ioctl(1, TIOCGWINSZ, &sz) == -1) return 80;
  return sz.ws_col;
#else
  return 80;
#endif
}


int rawtty_height (void) {
#ifdef TIOCGWINSZ
  struct winsize sz;
  if (ioctl(1, TIOCGWINSZ, &sz) == -1) return 25;
  return sz.ws_row;
#else
  return 25;
#endif
}


__attribute__((constructor)) static void ctor (void) {
  tcgetattr(STDIN_FILENO, &orig_tios);
}


__attribute__((destructor)) static void dtor (void) {
  rawtty_off();
}
