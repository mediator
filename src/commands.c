// <0: nothing; else: new value
static int cmdarg_oot (int flag, int argc, char *argv[]) {
  if (argc > 1) {
    int mt;
    char *s = cmdline_complete_abbrev(argv[1], 0, &mt, "on", "off", "toggle", NULL);
    if (mt == 1) {
      if (s[0] == 't') flag = !flag;
      else if (s[1] == 'n') flag = 1;
      else flag = 0;
    }
    free(s);
  }
  return flag;
}


static int cmdarg_oot_ac (int argc, char *argv[], int do_autocomplete) {
  if (do_autocomplete) {
    cmdline_addf("%s ", argv[0]);
    if (argc > 1) {
      int mt;
      char *s = cmdline_complete_abbrev(argv[1], 0, &mt, "on", "off", "toggle", NULL);
      cmdline_addf("%s%s", s, (mt == 1 ? " " : ""));
      free(s);
      if (mt != 1) {
        conlogf("%s options:\n", argv[0]);
        free(cmdline_complete_abbrev(argv[1], 1, &mt, "on", "off", "toggle", NULL));
      }
    } else {
      conlogf("%s options:\n on\n off\n toggle\n", argv[0]);
    }
    return 1;
  }
  return 0;
}


CMDLINE_CMD(help, "show help form commands") {
  CMDLINE_DEFAULT_AC();
  conlogf("help usage: 'help <commandname>'\n");
}

CMDLINE_CMD(quit, "quit mediator") {
  CMDLINE_DEFAULT_AC();
  do_shutdown = 3;
  raise(SIGINT);
}


CMDLINE_CMD(reload, "reload lists") {
  CMDLINE_DEFAULT_AC();
  do_hp_reload = 1;
}


CMDLINE_CMD(logtty, "on/off/toggle tty logs") {
  int flg;
  if (cmdarg_oot_ac(argc, argv, do_autocomplete)) return;
  flg = opt_logtty_disabled = !cmdarg_oot(!opt_logtty_disabled, argc, argv);
  conlogf("tty logs are %sabled\n", (flg ? "dis" : "en"));
}

CMDLINE_CMD(logfile, "on/off/toggle file logs") {
  int flg;
  if (cmdarg_oot_ac(argc, argv, do_autocomplete)) return;
  flg = opt_logfile_disabled = !cmdarg_oot(!opt_logfile_disabled, argc, argv);
  conlogf("file logs are %sabled\n", (flg ? "dis" : "en"));
}

CMDLINE_CMD(logs, "on/off/toggle logs") {
  int flg;
  if (cmdarg_oot_ac(argc, argv, do_autocomplete)) return;
  flg = opt_log_disabled = !cmdarg_oot(!opt_log_disabled, argc, argv);
  conlogf("logs are %sabled\n", (flg ? "dis" : "en"));
}


CMDLINE_CMD(new_log, "start new log") {
  CMDLINE_DEFAULT_AC();
  log_new();
}


CMDLINE_CMD(flush_log, "flush current log") {
  CMDLINE_DEFAULT_AC();
  log_flush();
}



CMDLINE_CMD(etags, "on/off/toggle etag stripping") {
  if (cmdarg_oot_ac(argc, argv, do_autocomplete)) return;
  opt_noetag = !cmdarg_oot(!opt_noetag, argc, argv);
  conlogf("etags are %sabled\n", (opt_noetag ? "dis" : "en"));
}


CMDLINE_CMD(hdrdump_server, "on/off/toggle server headers dumping") {
  if (cmdarg_oot_ac(argc, argv, do_autocomplete)) return;
  opt_dump_server_headers = cmdarg_oot(opt_dump_server_headers, argc, argv);
  conlogf("server headers dumping is %s\n", (opt_dump_server_headers ? "on" : "off"));
}

CMDLINE_CMD(hdrdump_client, "on/off/toggle client headers dumping") {
  if (cmdarg_oot_ac(argc, argv, do_autocomplete)) return;
  opt_dump_client_headers = cmdarg_oot(opt_dump_client_headers, argc, argv);
  conlogf("client headers dumping is %s\n", (opt_dump_client_headers ? "on" : "off"));
}


static void add_commands (void) {
}
