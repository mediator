__attribute__((unused)) static void ssl_debug (void *ctx, int level, const char *str) {
  ptlogf("SSL DEBUG(%d): %s\n", level, str);
}


/* no need to initialize cert */
static void cert_load_root (x509_crt *root_cert) {
  int ret;
  memset(root_cert, 0, sizeof(*root_cert));
  ret = x509_crt_parse(root_cert, fake_root_cert_data, fake_root_cert_data_size);
  if (ret != 0) {
    // the thing that should not be
    fprintf(stderr, "FUCKED: x509_crt_parse returned %d\n", ret);
    abort();
  }
}


static inline void cert_free (x509_crt *root_cert) {
  x509_crt_free(root_cert);
}


static int cert_gen (const char *hostname, char *cfname, x509_crt *root_cert, rsa_context *ssl_rsa) {
  x509_raw graw;
  char *hbuf = malloc(strlen(hostname)+8192);
  int res;
  //////////////////////////////////////////
  x509write_init_raw(&graw);
  //////////////////////////////////////////
  ptlogf("SSL: generating the certificate for host '%s'...\n", hostname);
  sprintf(hbuf, "CN=%s", hostname);
  //////////////////////////////////////////
  ptlogf("SSL: x509write_add_pubkey()\n");
  res = x509write_add_pubkey(&graw, ssl_rsa);
  if (res != 0) { ptlogf("SHIT: x509write_add_pubkey=%d\n", res); goto error; }
  //////////////////////////////////////////
  ptlogf("SSL: x509write_copy_issuer()\n");
  res = x509write_copy_issuer(&graw, root_cert);
  if (res != 0) { ptlogf("SHIT: x509write_copy_issuer=%d\n", res); goto error; }
  //////////////////////////////////////////
  ptlogf("SSL: x509write_add_subject()\n");
  res = x509write_add_subject(&graw, (unsigned char *)hbuf);
  if (res != 0) { ptlogf("SHIT: x509write_add_subject=%d\n", res); goto error; }
  //////////////////////////////////////////
  ptlogf("SSL: x509write_add_validity()\n");
  res = x509write_add_validity(&graw, (unsigned char *)"2013-01-01 12:00:00", (unsigned char *)"2016-01-01 12:00:00");
  if (res != 0) { ptlogf("SHIT: x509write_add_validity=%d\n", res); goto error; }
  //////////////////////////////////////////
  ptlogf("SSL: x509write_create_sign()\n");
  res = x509write_create_sign(&graw, ssl_rsa);
  if (res != 0) { ptlogf("SHIT: x509write_create_sign=%d\n", res); goto error; }
  //////////////////////////////////////////
  //sprintf(hbuf, "certs/cert.%s.pem", hostname);
  ptlogf("SSL: x509write_crtfile()\n");
  res = x509write_crtfile(&graw, cfname, X509_OUTPUT_PEM);
  if (res != 0) { ptlogf("SHIT: x509write_crtfile=%d\n", res); goto error; }
  //////////////////////////////////////////
  ptlogf("SSL: x509write_free_raw()\n");
  x509write_free_raw(&graw);
  //////////////////////////////////////////
  free(hbuf);
  return 0;
error:
  free(hbuf);
  x509write_free_raw(&graw);
  return -1;
}


// not thread-safe
static int cert_for_host (const char *hostname, x509_crt *root_cert, int fucked) {
  static char cfname[4096], hn[512];
  int ret;
  if (!fucked) {
    strcpy(hn, hostname);
  } else {
    snprintf(hn, sizeof(hn), "fuck.%s", hostname);
  }
  snprintf(cfname, sizeof(cfname), "certs/generated/cert.%s.pem", hn);
  if (access(cfname, R_OK) != 0) {
    ptlogf("no certificate for '%s'...\n", hn);
    if (cert_gen(hn, cfname, root_cert, &ssl_rsa) != 0) {
      ptlogf("FUCKED: can't generate certificate for '%s'!\n", hn);
      return -1;
    }
  } else {
    ptlogf("found certificate for '%s'...\n", hn);
  }
  ret = x509_crt_parse_file(root_cert, cfname);
  if (ret != 0) {
    ptlogf("FUCKED: x509_crt_parse_file returned %d\n", ret);
    return -1;
  }
  return 0;
}


/*
 * Read at most 'len' characters
 */
static int xssl_net_recv (void *ctx, unsigned char *buf, size_t len) {
  sock_t *sk = (sock_t *)ctx;
  if (sk->fd < 0) { errno = EBADF; return POLARSSL_ERR_NET_RECV_FAILED; }
  //ptlogf("XSSL_RECV: fd=%d; len=%u\n", sk->fd, len);
  int res = asnet_recv(sk->fd, buf, len, 0, (sk->is_client ? TIMEOUT_CLIENT : TIMEOUT_SERVER));
  //ptlogf("XSSL_RECV: fd=%d; len=%u; res=%d\n", sk->fd, len, res);
  if (res <= 0) {
    //POLARSSL_ERR_NET_WANT_READ: do it again
    if (res == 0) return POLARSSL_ERR_NET_CONN_RESET;
    return POLARSSL_ERR_NET_RECV_FAILED;
  }
  return res;
}


/*
 * Write 'len' characters
 */
static int xssl_net_send (void *ctx, const unsigned char *buf, size_t len) {
  sock_t *sk = (sock_t *)ctx;
  if (sk->fd < 0) return POLARSSL_ERR_NET_RECV_FAILED;
  //ptlogf("XSSL_SEND: fd=%d; len=%u\n", sk->fd, len);
  if (asnet_send(sk->fd, buf, len, (sk->is_client ? TIMEOUT_CLIENT : TIMEOUT_SERVER)) > 0) return (int)len;
  //ptlogf("XSSL_SEND: fd=%d; len=%u; FAILED!\n", sk->fd, len);
  //return POLARSSL_ERR_NET_CONN_RESET;
  return POLARSSL_ERR_NET_SEND_FAILED;
}
