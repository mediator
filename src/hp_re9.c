////////////////////////////////////////////////////////////////////////////////
static inline void trim_str (char *str) {
  while (str[0] && isspace(str[0])) memmove(str, str+1, strlen(str)+1);
  while (str[0] && isspace(str[strlen(str)-1])) str[strlen(str)-1] = 0;
}


////////////////////////////////////////////////////////////////////////////////
static inline int isGoodHostChar (char ch) {
  if (ch >= '0' && ch <= '9') return 1;
  if (ch >= 'a' && ch <= 'z') return 1;
  if (ch >= 'A' && ch <= 'Z') return 1;
  if (ch == '-') return 1;
  return 0;
}


static int isGoodHMPattern (const char *hm) {
  if (hm != NULL) {
    if (hm[0] == ':') return 0;
    if (hm[0] == '*') {
      while (*hm && *hm == '*') ++hm;
      if (hm[0] != '.') return 0;
      //while (*hm && *hm == '.') ++hm;
    }
    while (*hm) {
      //if (hm[0] == '*' || hm[0] == '?') return 0;
      if (hm[0] == ':') return 1;
      if (hm[0] == '.') {
        if (!isGoodHostChar(hm[1])) return 0;
      } else {
        if (!isGoodHostChar(hm[0])) return 0;
      }
      ++hm;
    }
    return 1;
  }
  return 0;
}



static int isHM (const char *pat, const char *host) {
  if (host != NULL && pat != NULL && host[0] && pat[0]) {
    int anySD = 0; // 1:can have any subdomain (or none at all), -1: MUST have subdomain
    if (pat[0] == '*') {
      anySD = 1;
      while (pat[0] && pat[0] == '*') ++pat;
      if (pat[0] != '.') return 0;
      while (pat[0] && pat[0] == '.') ++pat;
      if (!pat[0]) return 0;
    } else if (pat[0] == '.') {
      anySD = -1;
      while (pat[0] && pat[0] == '.') ++pat;
      if (!pat[0]) return 0;
    }
    int plen = strlen(pat);
    int hlen = strlen(host);
    if (plen > hlen) return 0; // pattern too long
    if (!anySD && plen != hlen) return 0; // can't match subdomains
    if (strcasecmp(pat, host+hlen-plen) != 0) return 0; // alas
    if (anySD) {
      if (anySD < 0 && hlen <= plen) return 0; // we need at least one subdomain
      // is it really subdomain?
      if (hlen > plen && host[hlen-plen-1] != '.') return 0;
    }
    return 1;
  }
  return 0;
}


/*
static int isHM (const char *pat, const char *host) {
  int res = isHMEx(pat, host);
  ptlogf("isHM: [%s] : [%s] : %d\n", pat, host, res);
  return res;
}
*/


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  http_method mt;
  int port; // <1: any
  char *litstr;
  re9_prog_t *re;
  char *newhost; // for host rewriting
} hostpattern_t;


typedef struct {
  hostpattern_t *hp;
  int used;
  int alloted;
} hostlist_t;


static hostlist_t host_blocks;
static hostlist_t host_allowrefs;
static hostlist_t host_blockuris;
static hostlist_t host_iframeuris;
static hostlist_t host_ipv4;
static hostlist_t host_ipv6;
static hostlist_t host_force_https;


static __attribute__((constructor)) void hp_ctor_ (void) {
  memset(&host_blocks, 0, sizeof(host_blocks));
  memset(&host_allowrefs, 0, sizeof(host_allowrefs));
  memset(&host_blockuris, 0, sizeof(host_blockuris));
  memset(&host_iframeuris, 0, sizeof(host_iframeuris));
  memset(&host_ipv4, 0, sizeof(host_ipv4));
  memset(&host_ipv6, 0, sizeof(host_ipv6));
  memset(&host_force_https, 0, sizeof(host_force_https));
}


static void hp_clear (hostlist_t *lst) {
  if (lst != NULL) {
    for (int f = 0; f < lst->used; ++f) {
      if (lst->hp[f].newhost != NULL) free(lst->hp[f].newhost);
      if (lst->hp[f].litstr != NULL) free(lst->hp[f].litstr);
      if (lst->hp[f].re != NULL) re9_free(lst->hp[f].re);
    }
    if (lst->hp != NULL) free(lst->hp);
    lst->hp = NULL;
    lst->used = lst->alloted = 0;
  }
}


static __attribute__((unused)) void hp_dump (const hostlist_t *lst) {
/*
  if (lst != NULL) {
    for (int f = 0; f < lst->used; ++f) {
      if (lst->hp[f].litstr) {
        ptlogf("%4d: [%s]\n", f, lst->hp[f].litstr);
      } else {
        ptlogf("%4d: regexp!\n", f);
      }
    }
  }
*/
}


static __attribute__((destructor)) void hp_dtor_ (void) {
  hp_clear(&host_blocks);
  hp_clear(&host_allowrefs);
  hp_clear(&host_blockuris);
  hp_clear(&host_iframeuris);
  hp_clear(&host_ipv4);
  hp_clear(&host_ipv6);
  hp_clear(&host_force_https);
}


static inline void hp_grow (hostlist_t *lst) {
  if (lst->used+1 > lst->alloted) {
    int newsz = ((lst->used+1)|0x7f)+1;
    lst->hp = realloc(lst->hp, sizeof(lst->hp[0])*newsz); //FIXME: check for errors
    for (int f = lst->used; f < newsz; ++f) memset(&lst->hp[f], 0, sizeof(lst->hp[f]));
    lst->alloted = newsz;
  }
  //ptlogf("hp_add: lst=%p (%p); idx=%d of %d; mt=%d; host=[%s]; port=%d (%p)\n", lst, lst->hp, lst->used, lst->alloted, mt, pat_copy, port, pat_copy);
  /*
  lst->hp[lst->used].mt = mt;
  lst->hp[lst->used].re = pat_copy;
  lst->hp[lst->used].port = port;
  ++lst->used;
  */
}


// starts with 'P:', 'G:', 'C:' -- methods
//TODO: add all non-star matches before star matches?
static void hp_add (hostlist_t *lst, const char *patstr) {
  if (patstr != NULL && patstr[0]) {
    const char *errmsg;
    char *col, *rephost = NULL;
    char *pat = alloca(strlen(patstr)+1);
    http_method mt = MT_ANY;
    int port = 0;
    int allowHM = 1;
    //
    strcpy(pat, patstr);
    if (pat[1] == ':') {
      switch (toupper(pat[0])) {
        case 'P': mt = MT_POST; pat += 2; break;
        case 'G': mt = MT_GET; pat += 2; break;
        case 'C': mt = MT_CONNECT; pat += 2; break;
        case 'A': mt = MT_ANY; pat += 2; break;
      }
    }
    // process replacement host
    // host => newhost
    if ((col = strstr(pat, "=>")) != NULL) {
      char *cc = col;
      while (cc > pat && cc[-1] == '=') --cc;
      while (cc > pat && isspace(cc[-1])) --cc;
      *cc = 0; // cut it out
      col += 2;
      while (*col && isspace(*col)) ++col;
      while (*col && isspace(col[strlen(col)-1])) col[strlen(col)-1] = 0;
      if (*col && cc > pat) rephost = col;
      allowHM = 0;
    }
    // process port
    if (pat[1] && (col = strrchr(pat+1, ':')) != NULL) {
      ++col;
      port = atoi(col); //FIXME!
      if (port < 1 || port > 65535) {
        fprintf(stderr, "WARNING: ignored pattern with invalid port '%s'\n", pat);
        return;
      }
      --col;
    } else {
      col = pat+strlen(pat);
    }
    // now add properly escaped host pattern
    //if (pat[0] == '*') while (*pat && pat[1] == '*') ++pat;
    if (pat[0] == ':') {
      // regular expression
      char *pt;
      if (*(++pat) == 0) return;
      pt = strdup(pat);
      if (col[0]) strrchr(pt, ':')[0] = 0; // simple hack
      hp_grow(lst);
      lst->hp[lst->used].mt = mt;
      lst->hp[lst->used].port = port;
      lst->hp[lst->used].litstr = NULL;
      lst->hp[lst->used].re = re9_compile(pt, RE9_FLAG_NONUTF8, &errmsg);
      free(pt);
      if (lst->hp[lst->used].re == NULL) {
        _ptlog(NULL, "FATAL: invalid regexp '%s': %s\n", pat, errmsg);
      } else {
        lst->hp[lst->used].newhost = (rephost != NULL ? strdup(rephost) : NULL);
        ++lst->used;
      }
    } else if (allowHM && isGoodHMPattern(pat)) {
      // simple HM
      int insbef = 0;
      for (int f = 0; f < lst->used; ++f) if (lst->hp[f].litstr == NULL) { insbef = f; break; }
      hp_grow(lst);
      if (insbef < lst->used) for (int f = lst->used; f > insbef; --f) lst->hp[f] = lst->hp[f-1];
      ++lst->used;
      lst->hp[insbef].mt = mt;
      lst->hp[insbef].port = port;
      lst->hp[insbef].litstr = strdup(pat);
      lst->hp[insbef].re = NULL;
      lst->hp[insbef].newhost = (rephost != NULL ? strdup(rephost) : NULL);
      if (col[0]) strchr(lst->hp[insbef].litstr, ':')[0] = 0; // simple hack
      //ptlogf("HM: [%s]\n", lst->hp[insbef].litstr);
    } else {
      // convert to regexp
      xstrbuf_t ps;
      xstrbuf_init(&ps);
      xstrbuf_addc(&ps, '^');
      if (pat[0] == '*' && pat[1] == '.') {
        // special case
        xstrbuf_addstr(&ps, "(?:.*\\.)?");
        pat += 2;
      } else if (pat[0] == '.') {
        // another special case
        xstrbuf_addstr(&ps, ".*\\.");
        pat += 1;
      }
      // now copy pattern, escaping chars and adding [^.] before '*'
      while (pat < col) {
        switch (*pat) {
          case '.':
          case '+': case '?':
          case '[': case ']':
          case '(': case ')':
          case '|': case '\\':
          case '^': case '$':
            xstrbuf_addc(&ps, '\\');
            xstrbuf_addc(&ps, *pat);
            break;
          case '*':
            while (pat < col && *pat == '*') ++pat;
            --pat;
            xstrbuf_addstr(&ps, ".*"); // maybe use "[^.]" instead of "."?
            break;
          default:
            xstrbuf_addc(&ps, *pat);
            break;
        }
        ++pat;
      }
      xstrbuf_addc(&ps, '$');
      hp_grow(lst);
      lst->hp[lst->used].mt = mt;
      lst->hp[lst->used].port = port;
      lst->hp[lst->used].litstr = NULL;
      lst->hp[lst->used].re = re9_compile(ps.data, RE9_FLAG_NONUTF8, &errmsg);
      if (lst->hp[lst->used].re == NULL) {
        _ptlog(NULL, "FATAL: invalid regexp '%s': %s\n", ps.data, errmsg);
      } else {
        lst->hp[lst->used].newhost = (rephost != NULL ? strdup(rephost) : NULL);
        ++lst->used;
      }
      xstrbuf_free(&ps);
    }
  }
}


// NOT PTHREAD-SAFE!
static int hp_in_list (const hostlist_t *lst, http_method mt, const char *host, int port) {
  for (int f = 0; f < lst->used; ++f) {
    //fprintf(stderr, "checking: mt=%d; host=%s; port=%d : mt=%d; host=%s; port=%d\n", mt, host, port, lst->hp[f].mt, lst->hp[f].re, lst->hp[f].port);
    if (lst->hp[f].mt != MT_ANY && lst->hp[f].mt != mt) continue;
    if (lst->hp[f].port != 0 && lst->hp[f].port != port) continue;
    if (lst->hp[f].litstr != NULL) {
      if (isHM(lst->hp[f].litstr, host)) return 1;
    } else {
      if (re9_execute(lst->hp[f].re, RE9_FLAG_NONUTF8, host, NULL, 0)) return 1;
    }
    //fprintf(stderr, " HIT!\n");
  }
  //fprintf(stderr, " NO HIT!\n");
  return 0;
}


// NOT PTHREAD-SAFE!
static const char *hp_in_list_newhost (const hostlist_t *lst, http_method mt, const char *host, int port) {
  for (int hpnum = 0; hpnum < lst->used; ++hpnum) {
    //fprintf(stderr, "checking: mt=%d; host=%s; port=%d : mt=%d; host=%s; port=%d\n", mt, host, port, lst->hp[hpnum].mt, lst->hp[hpnum].re, lst->hp[hpnum].port);
    if (lst->hp[hpnum].mt != MT_ANY && lst->hp[hpnum].mt != mt) continue;
    if (lst->hp[hpnum].port != 0 && lst->hp[hpnum].port != port) continue;
    if (lst->hp[hpnum].litstr != NULL) {
      if (isHM(lst->hp[hpnum].litstr, host)) return (lst->hp[hpnum].newhost != NULL ? lst->hp[hpnum].newhost : host);
    } else {
      static re9_sub_t mt[16];
      if (re9_execute(lst->hp[hpnum].re, RE9_FLAG_NONUTF8, host, mt, 16)) {
        static char rep[1024];
        if (lst->hp[hpnum].newhost == NULL) return host;
        re9_subst(rep, sizeof(rep), lst->hp[hpnum].newhost, mt, re9_nsub(lst->hp[hpnum].re));
        return rep;
      }
    }
    //fprintf(stderr, " HIT!\n");
  }
  //fprintf(stderr, " NO HIT!\n");
  return NULL;
}


static void hp_load (hostlist_t *lst, const char *fname) {
  FILE *fl = fopen(fname, "r");
  hp_clear(lst);
  if (fl != NULL) {
    char *str = malloc(8192);
    //ptlogf("FILE: [%s]\n", fname);
    while (fgets(str, 8191, fl) != NULL) {
      trim_str(str);
      if (!str[0] || str[0] == '#') continue; // skip comments and empty lines
      hp_add(lst, str);
    }
    free(str);
    fclose(fl);
  }
}


static void hp_add_re (hostlist_t *lst, const char *re) {
  const char *errmsg;
  http_method mt = MT_ANY;
  if (re[1] == ':') {
    switch (toupper(re[0])) {
      case 'P': mt = MT_POST; re += 2; break;
      case 'G': mt = MT_GET; re += 2; break;
      case 'C': mt = MT_CONNECT; re += 2; break;
      case 'A': mt = MT_ANY; re += 2; break;
    }
  }
  hp_grow(lst);
  lst->hp[lst->used].mt = mt;
  lst->hp[lst->used].port = 0;
  lst->hp[lst->used].litstr = NULL;
  lst->hp[lst->used].re = re9_compile(re, RE9_FLAG_NONUTF8, &errmsg);
  if (lst->hp[lst->used].re == NULL) {
    _ptlog(NULL, "FATAL: invalid regexp '%s': %s\n", re, errmsg);
  } else {
    ++lst->used;
  }
}


static void hp_load_re (hostlist_t *lst, const char *fname) {
  FILE *fl = fopen(fname, "r");
  hp_clear(lst);
  if (fl != NULL) {
    char *str = malloc(8192);
    //ptlogf("FILE: [%s]\n", fname);
    while (fgets(str, 8191, fl) != NULL) {
      trim_str(str);
      if (!str[0] || str[0] == '#') continue; // skip comments and empty lines
      hp_add_re(lst, str);
    }
    free(str);
    fclose(fl);
  }
}


static int hp_check_re_uri (hostlist_t *lst, http_method mt, const char *proto, const char *hostname, int port, const char *path) {
  static char *uri = NULL, pstr[16];
  static int uri_size = 0;
  int len;
  if (path == NULL || !path[0]) path = "/";
  if ((port == 80 && strcmp(proto, "http") == 0) || (port == 443 && strcmp(proto, "https") == 0)) {
    pstr[0] = 0;
  } else {
    snprintf(pstr, sizeof(pstr), ":%d", port);
  }
  len = strlen(proto)+strlen(hostname)+strlen(pstr)+strlen(path)+8;
  if (len > uri_size) {
    uri = realloc(uri, len); //FIXME: check for errors
    uri_size = len;
  }
  //_ptlog(NULL, "\3checking uri: proto=[%s]; hostname=[%s]; pstr=[%s]; path=[%s]\n", proto, hostname, pstr, path);
  sprintf(uri, "%s://%s%s%s", proto, hostname, pstr, path);
  //_ptlog(NULL, "\3checking uri: [%s]\n", uri);
  for (int hpnum = 0; hpnum < lst->used; ++hpnum) {
    if (lst->hp[hpnum].mt != MT_ANY && lst->hp[hpnum].mt != mt) continue;
    if (lst->hp[hpnum].litstr != NULL) {
      if (isHM(lst->hp[hpnum].litstr, uri)) return 1;
    } else {
      if (re9_execute(lst->hp[hpnum].re, RE9_FLAG_NONUTF8, uri, NULL, 0)) return 1;
    }
  }
  return 0;
}


static inline int is_blocked_uri (http_method mt, const char *proto, const char *hostname, int port, const char *path) {
  return hp_check_re_uri(&host_blockuris, mt, proto, hostname, port, path);
}


static inline int is_iframe_uri (http_method mt, const char *proto, const char *hostname, int port, const char *path) {
  return hp_check_re_uri(&host_iframeuris, mt, proto, hostname, port, path);
}


static inline int is_v4_host (http_method mt, const char *hostname, int port) {
  return hp_in_list(&host_ipv4, mt, hostname, port);
}


static inline int is_v6_host (http_method mt, const char *hostname, int port) {
  return hp_in_list(&host_ipv6, mt, hostname, port);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  char *repl;
  http_method mt;
  re9_prog_t *re;
  int re_flags;
} repattern_t;


typedef struct {
  repattern_t *pat;
  int used;
  int alloted;
} relist_t;


static relist_t re_uareplace;


static __attribute__((constructor)) void relist_ctor_ (void) {
  memset(&re_uareplace, 0, sizeof(re_uareplace));
}


static void relist_clear (relist_t *lst) {
  if (lst != NULL) {
    for (int f = 0; f < lst->used; ++f) {
      if (lst->pat[f].repl != NULL) free(lst->pat[f].repl);
      if (lst->pat[f].re != NULL) re9_free(lst->pat[f].re);
    }
    if (lst->pat != NULL) free(lst->pat);
    lst->pat = NULL;
    lst->used = lst->alloted = 0;
  }
}


static __attribute__((destructor)) void relist_dtor_ (void) {
  relist_clear(&re_uareplace);
}




static inline void relist_grow (relist_t *lst) {
  if (lst->used+1 > lst->alloted) {
    int newsz = ((lst->used+1)|0x7f)+1;
    lst->pat = realloc(lst->pat, sizeof(lst->pat[0])*newsz); //FIXME: check for errors
    lst->alloted = newsz;
  }
}


// if `pat` starts with 'P:', 'G:', 'C:' -- methods
static void relist_add (relist_t *lst, const char *pat, const char *repl) {
  if (pat != NULL && pat[0]) {
    const char *errmsg;
    http_method mt = MT_ANY;
    re9_prog_t *re;
    int flags = RE9_FLAG_NONUTF8;
    //
    if (pat[1] == ':') {
      switch (toupper(pat[0])) {
        case 'P': mt = MT_POST; pat += 2; break;
        case 'G': mt = MT_GET; pat += 2; break;
        case 'C': mt = MT_CONNECT; pat += 2; break;
        case 'A': mt = MT_ANY; pat += 2; break;
      }
    }
    if (pat[0] == '/' && strchr(pat+1, '/') != NULL) {
      /* regexp options */
      for (++pat; *pat != '/'; ++pat) {
        switch (*pat) {
          case 'i': flags |= RE9_FLAG_CASEINSENS; break;
          case 'I': flags &= ~RE9_FLAG_CASEINSENS; break;
          case 'u': flags &= ~RE9_FLAG_NONUTF8; break;
          case 'U': flags |= RE9_FLAG_NONUTF8; break;
          case 'l': flags |= RE9_FLAG_LITERAL; break;
          case 'L': flags &= ~RE9_FLAG_LITERAL; break;
        }
      }
      ++pat;
    }
    if (!pat[0]) {
      _ptlog(NULL, "FATAL: empty pattern!\n");
      return;
    }
    if ((re = re9_compile(pat, flags, &errmsg)) == NULL) {
      _ptlog(NULL, "FATAL: invalid regexp '%s': %s\n", pat, errmsg);
      return;
    }
    //_ptlog(NULL, "RELIST: flags=0x%04x; re=[%s]; repl=[%s]\n", flags, pat, repl);
    relist_grow(lst);
    lst->pat[lst->used].mt = mt;
    lst->pat[lst->used].repl = (repl != NULL ? strdup(repl) : NULL);
    lst->pat[lst->used].re = re;
    lst->pat[lst->used].re_flags = flags;
    ++lst->used;
  }
}


// NOT PTHREAD-SAFE!
static repattern_t *relist_find (relist_t *lst, http_method mt, const char *proto, const char *hostname, int port, const char *path) {
  static char *uri = NULL, pstr[16];
  static int uri_size = 0;
  int len;
  if (path == NULL || !path[0]) path = "/";
  if ((port == 80 && strcmp(proto, "http") == 0) || (port == 443 && strcmp(proto, "https") == 0)) {
    pstr[0] = 0;
  } else {
    snprintf(pstr, sizeof(pstr), ":%d", port);
  }
  len = strlen(proto)+strlen(hostname)+strlen(pstr)+strlen(path)+8;
  if (len > uri_size) {
    uri = realloc(uri, len); //FIXME: check for errors
    uri_size = len;
  }
  sprintf(uri, "%s://%s%s%s", proto, hostname, pstr, path);
  //_ptlog(NULL, "RELIST: checking uri: [%s]\n", uri);
  for (int f = 0; f < lst->used; ++f) {
    if (lst->pat[f].mt != MT_ANY && lst->pat[f].mt != mt) continue;
    if (re9_execute(lst->pat[f].re, lst->pat[f].re_flags, uri, NULL, 0)) {
      //_ptlog(NULL, " RELIST HIT: flags=0x%04x; repl=[%s]\n", lst->pat[f].re_flags, lst->pat[f].repl);
      return &lst->pat[f];
    }
  }
  return NULL;
}


static void relist_load_ua (relist_t *lst, const char *fname) {
  FILE *fl = fopen(fname, "r");
  relist_clear(lst);
  if (fl != NULL) {
    char *str = malloc(8192);
    char *last_ua = NULL;
    //ptlogf("FILE: [%s]\n", fname);
    while (fgets(str, 8191, fl) != NULL) {
      trim_str(str);
      if (str[0] == '#' || !str[0]) continue; // skip comments and empty lines
      if (strncmp(str, "ua=", 3) == 0) {
        /* new UA */
        if (last_ua != NULL) free(last_ua);
        last_ua = NULL;
        if (!str[3]) continue;
        last_ua = strdup(str+3);
        continue;
      }
      /* new regexp */
      if (last_ua != NULL) {
        relist_add(lst, str, (strcmp(last_ua, "-") == 0 ? NULL : last_ua));
      }
    }
    free(str);
    if (last_ua != NULL) free(last_ua);
    fclose(fl);
  }
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  char *repl;
  int use_location;
  int dont_stop; // process all list again?
  re9_prog_t *re;
  int re_flags;
} rewrite_t;


typedef struct {
  rewrite_t *pat;
  int used;
  int alloted;
} rewrite_list_t;


static rewrite_list_t re_rewrites;


static __attribute__((constructor)) void rewrite_list_ctor_ (void) {
  memset(&re_rewrites, 0, sizeof(re_rewrites));
}


static void rewrite_list_clear (rewrite_list_t *lst) {
  if (lst != NULL) {
    for (int f = 0; f < lst->used; ++f) {
      if (lst->pat[f].repl != NULL) free(lst->pat[f].repl);
      if (lst->pat[f].re != NULL) re9_free(lst->pat[f].re);
    }
    if (lst->pat != NULL) free(lst->pat);
    lst->pat = NULL;
    lst->used = lst->alloted = 0;
  }
}


static __attribute__((destructor)) void rewrite_list_dtor_ (void) {
  rewrite_list_clear(&re_rewrites);
}


static inline void rewrite_list_grow (rewrite_list_t *lst) {
  if (lst->used+1 > lst->alloted) {
    int newsz = ((lst->used+1)|0x7f)+1;
    lst->pat = realloc(lst->pat, sizeof(lst->pat[0])*newsz); //FIXME: check for errors
    lst->alloted = newsz;
  }
}


static void rewrite_list_add (rewrite_list_t *lst, const char *pat) {
  if (pat != NULL && pat[0]) {
    const char *errmsg;
    re9_prog_t *re;
    int flags = RE9_FLAG_NONUTF8;
    char *pat_str;
    const char *rep_str, *orig_pat = pat;
    int use_loc = 0;
    int dont_stop = 0;
    //
    if (pat[0] == '<') {
      dont_stop = 1;
      ++pat;
    }
    if (pat[0] == '/' && strchr(pat+1, '/') != NULL) {
      /* regexp options */
      for (++pat; *pat != '/'; ++pat) {
        switch (*pat) {
          case 'i': flags |= RE9_FLAG_CASEINSENS; break;
          case 'I': flags &= ~RE9_FLAG_CASEINSENS; break;
          case 'u': flags &= ~RE9_FLAG_NONUTF8; break;
          case 'U': flags |= RE9_FLAG_NONUTF8; break;
          case 'l': flags |= RE9_FLAG_LITERAL; break;
          case 'L': flags &= ~RE9_FLAG_LITERAL; break;
        }
      }
      ++pat;
    }
    if (!pat[0]) {
      _ptlog(NULL, "FATAL: empty pattern!\n");
      return;
    }
    /* find replacement string */
    rep_str = strchr(pat, '>');
    use_loc = 0;
    while (rep_str != NULL) {
      const char *p = rep_str-1;
      use_loc = 0;
      if (p >= pat && *p == ':') { use_loc = 1; --p; }
      if (p >= pat && *p == '=') {
        while (p >= pat && *p == '=') --p;
        while (p > pat && isspace(p[-1])) --p;
        ++rep_str;
        while (*rep_str && isspace(*rep_str)) ++rep_str;
        if (p > pat && *rep_str) {
          /* found "=>"; p points to first space */
          pat_str = calloc(1, p-pat+1);
          memcpy(pat_str, pat, p-pat);
          break;
        }
      } else {
        ++rep_str;
      }
      rep_str = strchr(rep_str, '>');
    }
    if (rep_str == NULL) {
      _ptlog(NULL, "FATAL: pattern without replacement: [%s]\n", orig_pat);
      return;
    }
    if ((re = re9_compile(pat_str, flags, &errmsg)) == NULL) {
      _ptlog(NULL, "FATAL: invalid regexp '%s': %s\n", pat_str, errmsg);
      free(pat_str);
      return;
    }
    free(pat_str);
    rewrite_list_grow(lst);
    lst->pat[lst->used].repl = strdup(rep_str);
    lst->pat[lst->used].use_location = use_loc;
    lst->pat[lst->used].dont_stop = dont_stop;
    lst->pat[lst->used].re = re;
    lst->pat[lst->used].re_flags = flags;
    ++lst->used;
  }
}


// NOT PTHREAD-SAFE!
static const char *rewrite_list_find (rewrite_list_t *lst, const char *proto, const char *hostname, int port, const char *path, int *ret_location, int *again) {
  static char *uri = NULL, pstr[16];
  static int uri_size = 0;
  int len;
  if (ret_location != NULL) *ret_location = 0;
  if (again != NULL) *again = 0;
  if (path == NULL || !path[0]) path = "/";
  if ((port == 80 && strcmp(proto, "http") == 0) || (port == 443 && strcmp(proto, "https") == 0)) {
    pstr[0] = 0;
  } else {
    snprintf(pstr, sizeof(pstr), ":%d", port);
  }
  len = strlen(proto)+strlen(hostname)+strlen(pstr)+strlen(path)+8;
  if (len > uri_size) {
    uri = realloc(uri, len); //FIXME: check for errors
    uri_size = len;
  }
  sprintf(uri, "%s://%s%s%s", proto, hostname, pstr, path);
  //_ptlog(NULL, "RELIST: checking uri: [%s]\n", uri);
  for (int hpnum = 0; hpnum < lst->used; ++hpnum) {
    static re9_sub_t mt[RE9_SUBEXP_MAX];
    if (re9_execute(lst->pat[hpnum].re, lst->pat[hpnum].re_flags, uri, mt, RE9_SUBEXP_MAX)) {
      static char rep[1024];
      re9_subst(rep, sizeof(rep), lst->pat[hpnum].repl, mt, re9_nsub(lst->pat[hpnum].re));
      if (ret_location != NULL) *ret_location = lst->pat[hpnum].use_location;
      //_ptlog(NULL, "\x04REWRITE: [%s] ==> [%s]\n", uri, rep);
      if (again != NULL) *again = lst->pat[hpnum].dont_stop;
      return rep;
    }
  }
  return NULL;
}


static void rewrite_list_load (rewrite_list_t *lst, const char *fname) {
  FILE *fl = fopen(fname, "r");
  rewrite_list_clear(lst);
  if (fl != NULL) {
    char *str = malloc(8192);
    while (fgets(str, 8191, fl) != NULL) {
      trim_str(str);
      if (str[0] == '#' || !str[0]) continue; // skip comments and empty lines
      rewrite_list_add(lst, str);
    }
    free(str);
    fclose(fl);
  }
}
