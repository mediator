/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */
#include "xstrbuf.h"

#include <stdlib.h>
#include <string.h>


////////////////////////////////////////////////////////////////////////////////
// <0: error; 0: ok
int xstrbuf_init (xstrbuf_t *b) {
  if (b != NULL) {
    memset(b, 0, sizeof(*b));
    if ((b->data = malloc(sizeof(char))) == NULL) return -1;
    b->size = 1;
    b->data[0] = 0;
    return 0;
  }
  return -1;
}


// <0: error; 0: ok
int xstrbuf_free (xstrbuf_t *b) {
  if (b != NULL) {
    if (b->data != NULL) free(b->data);
    memset(b, 0, sizeof(*b));
    return 0;
  }
  return -1;
}


// <0: error; 0: ok
int xstrbuf_addc (xstrbuf_t *b, char c) {
  if (b != NULL) {
    if (b->used+2 >= b->size) {
      int newsz = ((b->used+2+0x3ff)|0x3ff)+1;
      char *n = realloc(b->data, newsz);
      if (n == NULL) return -1;
      b->data = n;
      b->size = newsz;
    }
    b->data[b->used++] = c;
    b->data[b->used] = 0;
    return 0;
  }
  return -1;
}


// <0: error; 0: ok
int xstrbuf_addstr (xstrbuf_t *b, const char *s) {
  if (s == NULL) return 0;
  if (b != NULL) {
    while (*s) {
      int res = xstrbuf_addc(b, *s++);
      if (res) return res;
    }
    return 0;
  }
  return -1;
}
