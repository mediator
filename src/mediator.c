#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "libre9/re9.h"
#include "libk8clock/k8clock.h"
#include "libcoro/coro.h"

#include "libpolarssl/polarssl/config.h"
#include "libpolarssl/polarssl/entropy.h"
#include "libpolarssl/polarssl/ctr_drbg.h"
#include "libpolarssl/polarssl/certs.h"
#include "libpolarssl/polarssl/x509.h"
#include "libpolarssl/polarssl/x509_crt.h"
#include "libpolarssl/polarssl/x509write.h"
#include "libpolarssl/polarssl/ssl.h"
#include "libpolarssl/polarssl/net.h"
#include "libpolarssl/polarssl/timing.h"
#include "libpolarssl/polarssl/rsa.h"
#include "libpolarssl/polarssl/pk.h"

#include "rawtty.h"

//#include "hostmatch.h"
#include "xstrbuf.h"
#include "crackurl.h"


////////////////////////////////////////////////////////////////////////////////
static entropy_context ssl_entropy_cli, ssl_entropy_srv;
static ctr_drbg_context ssl_ctr_drbg_cli, ssl_ctr_drbg_srv;
static pk_context ssl_pkey;
static rsa_context ssl_rsa;

static unsigned char *fake_root_cert_data = NULL;
static size_t fake_root_cert_data_size;


__attribute__((destructor)) static void fake_root_dtor_ (void) {
  if (fake_root_cert_data != NULL) {
    free(fake_root_cert_data);
    rsa_free(&ssl_rsa);
    pk_free(&ssl_pkey);
  }
}


static int load_file (const char *path, unsigned char **buf, size_t *n) {
  FILE *fl;
  if ((fl = fopen(path, "r")) == NULL) return -1;
  fseek(fl, 0, SEEK_END);
  *n = (size_t)ftell(fl);
  fseek(fl, 0, SEEK_SET);
  if ((*buf = calloc(1, *n+1)) == NULL) { fclose(fl); return -1; }
  if (fread(*buf, 1, *n, fl) != *n) {
    fclose(fl);
    free(*buf);
    return -1;
  }
  fclose(fl);
  (*buf)[*n] = 0;
  return 0;
}


static void cert_init (void) {
  static const char *pers0 = "fuckme";
  static const char *pers1 = "fuckhim";
  x509_crt root_cert;
  int ret;

  entropy_init(&ssl_entropy_cli);
  if ((ret = ctr_drbg_init(&ssl_ctr_drbg_cli, entropy_func, &ssl_entropy_cli, (const void *)pers0, strlen(pers0))) != 0) {
    fprintf(stderr, "FUCKED: ctr_drbg_init(cli) returned %d\n", ret);
    goto exit;
  }

  entropy_init(&ssl_entropy_srv);
  if ((ret = ctr_drbg_init(&ssl_ctr_drbg_srv, entropy_func, &ssl_entropy_srv, (const void *)pers1, strlen(pers1))) != 0) {
    fprintf(stderr, "FUCKED: ctr_drbg_init(srv) returned %d\n", ret);
    goto exit;
  }

  pk_init(&ssl_pkey);
  ret = pk_parse_keyfile(&ssl_pkey, "certs/key.pem", NULL);
  if (ret != 0) {
    fprintf(stderr, "FUCKED: pk_parse_keyfile returned %d\n", ret);
    goto exit;
  }
  if (!pk_can_do(&ssl_pkey, POLARSSL_PK_RSA)) {
    fprintf(stderr, "FUCKED: pk_can_do returned %d\n", ret);
    goto exit;
  }

  // i need rsa for certificate generator (it's not ported to new PolarSSL yet)
  rsa_init(&ssl_rsa, RSA_PKCS_V15, 0);
  rsa_copy(&ssl_rsa, pk_rsa(ssl_pkey));

  if (load_file("certs/cert.pem", &fake_root_cert_data, &fake_root_cert_data_size) != 0) {
    fprintf(stderr, "FUCKED: can't load 'certs/cert.pem'\n");
    goto exit;
  }

  memset(&root_cert, 0, sizeof(root_cert));
  ret = x509_crt_parse(&root_cert, fake_root_cert_data, fake_root_cert_data_size);
  if (ret != 0) {
    fprintf(stderr, "FUCKED: x509_crt_parse returned %d\n", ret);
    goto exit;
  }

  x509_crt_free(&root_cert);
  return;

exit:
  abort();
}


////////////////////////////////////////////////////////////////////////////////
#define ARRAYLEN(_a)  (sizeof(_a)/sizeof((_a)[0]))


////////////////////////////////////////////////////////////////////////////////
#define MAX_FDWAITS  (4)
typedef enum {
  SAR_NONE = 0,
  SAR_READ = 0x01,
  SAR_WRITE = 0x02,
  SAR_TIMEOUT = 0x04, // ignored before select()
} fd_flags;


typedef struct {
  int fd;
  fd_flags flags; // SAR_xxx
  uint64_t expire_time_milli; // timeout; 0: never
} fd_info_t;


typedef struct {
  char *buf;
  int size, used;
  uint64_t total;
} xbuf_t;


//TODO: add 'terminator callback', which will clean thread resources
//TODO: 'terminator callback' needs void *udata
typedef struct coro_info_s {
  struct coro_info_s *prev, *next;
  int tid;
  coro_context ctx;
  struct coro_stack stk;
  xbuf_t bufs[2]; // preallocated
  fd_info_t waits[MAX_FDWAITS]; // automatically cleared by sheduler
  fd_info_t wres[MAX_FDWAITS]; // filled by sheduler
  // udata, actually
  int clifd; // -666: destroyed
  // async resolver
  int resolving_name; // on call: set to 1; on return: <0: error; 0:ok, examine and free res_addr
  struct addrinfo hints;
  struct gaicb gacb;
  struct gaicb *gacbarr;
  struct sigevent sig;
  // sheduler flags
  int rc_state; // 0: don't touch; <0: just 'refill'; >0: shedule (was event)
  // sleep flags
  uint64_t sleep_expire_time_milli; // 0: don't sleeping
} coro_info_t;


////////////////////////////////////////////////////////////////////////////////
static int opt_proxy_port = 8090;
static int opt_dump_client_headers = 0;
static int opt_dump_server_headers = 0;
static int opt_noetag = 1; // fuck etags by default


////////////////////////////////////////////////////////////////////////////////
typedef enum {
  MT_GET,
  MT_POST,
  MT_HEAD,
  MT_OPTIONS,
  MT_CONNECT,
  MT_ANY // for matcher
} http_method;


static const char *mt_names[] = { "GET", "POST", "HEAD", "OPTIONS", "CONNECT" };
////////////////////////////////////////////////////////////////////////////////
static volatile sig_atomic_t do_shutdown = 0;
static volatile sig_atomic_t do_hp_reload = 0;


/*
static void sighandler (int signum, siginfo_t *info, void *ptr) {
  ++do_shutdown;
}


static void sigusr2handler (int signum, siginfo_t *info, void *ptr) {
  ++do_hp_reload;
}
*/


static int setup_resolver_signalfd (void) {
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGRTMIN);
  //sigprocmask(SIG_BLOCK, &mask, NULL); //we block the signal
  pthread_sigmask(SIG_BLOCK, &mask, NULL); //we block the signal
  return signalfd(-1, &mask, SFD_NONBLOCK);
}


////////////////////////////////////////////////////////////////////////////////
static inline int buf_remove_header (char *buf, const char *name) {
  int len = strlen(name);
  int res = 0;
  for (;;) {
    buf = strchr(buf+1, '\n');
    if (buf == NULL) break;
again:
    if (strncasecmp(buf+1, name, len) == 0) {
      // got it
      char *b2 = strchr(buf+1, '\n');
      if (b2 == NULL) break;
      memmove(buf, b2, strlen(b2)+1);
      ++res;
      goto again;
    }
  }
  return res;
}


// return either NULL or data start
static inline char *buf_find_header (char *buf, const char *name) {
  int len = strlen(name);
  for (;;) {
    buf = strchr(buf+1, '\n');
    if (buf == NULL) break;
    if (strncasecmp(buf+1, name, len) == 0) {
      // got it
      char *b2 = strchr(buf+1, ':');
      if (b2 == NULL) break;
      ++b2;
      while (*b2) {
        if (*b2 == '\r' || *b2 == '\n') return b2;
        if ((unsigned char)(*b2) <= 32) ++b2; else return b2;
      }
    }
  }
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
#define TIMEOUT_SERVER  (20000)
#define TIMEOUT_CLIENT  (3500)
#define TIMEOUT_DNS     (2000)

static void proxy_thread (void *arg); // arg is (coro_info_t *)


////////////////////////////////////////////////////////////////////////////////
static inline void mcsecs_to_timeval (struct timeval *to, int64_t mcsec) {
  if (mcsec < 0) mcsec = 0;
  to->tv_sec = mcsec/1000000;
  to->tv_usec = mcsec%1000000;
}


static inline void mssecs_to_timeval (struct timeval *to, int mssec) {
  if (mssec < 0) mssec = 0;
  to->tv_sec = mssec/1000;
  to->tv_usec = (int64_t)(mssec%1000)*1000;
}


static inline void socket_set_nonblocking (int fd) {
  int flags = fcntl(fd, F_GETFL, 0);
  fcntl(fd, F_SETFL, flags|O_NONBLOCK);
}


////////////////////////////////////////////////////////////////////////////////
static inline void skclose (int fd) {
  if (fd >= 0) close(fd);
}


static void skabort (int fd) {
  if (fd >= 0) {
    struct linger lg;
    lg.l_onoff = 1;
    lg.l_linger = 0;
    setsockopt(fd, SOL_SOCKET, SO_LINGER, &lg, sizeof(lg));
    close(fd);
  }
}


////////////////////////////////////////////////////////////////////////////////
#include "log.c"
#include "coro_man.c"
#include "cmdline.c"


////////////////////////////////////////////////////////////////////////////////
#include "hp_re9.c"
#include "hype.c"


static inline int is_referer_allowed (http_method mt, const char *hostname, int port) {
  return hp_in_list(&host_allowrefs, mt, hostname, port);
}


static inline const char *is_https_host (http_method mt, const char *hostname, int port) {
  return hp_in_list_newhost(&host_force_https, mt, hostname, port);
}


static inline int is_forbidden_host (http_method mt, const char *hostname, int port) {
  /*
  ptlogf("is forbidden [%s]?", hostname);
  int res = hp_in_list(&host_blocks, mt, hostname, port);
  ptlogf("is forbidden [%s] == %d", hostname, res);
  return res;
  */
  return hp_in_list(&host_blocks, mt, hostname, port);
}


////////////////////////////////////////////////////////////////////////////////
#include "sheduler.c"
#include "net_io.c"

#include "gencert.c"

#include "proxy.c"

#include "commands.c"


int main (int argc, char *argv[]) {
  if (argc > 1) {
    int port = atoi(argv[1]);
    if (port < 1 || port > 65535) {
      fprintf(stderr, "usage: %s [port]\ndefault port is %d\n", argv[0], opt_proxy_port);
      return -1;
    }
    opt_proxy_port = port;
    opt_dump_client_headers = 1;
    opt_dump_server_headers = 1;
    strcpy(log_name, "zxlog.log");
  }
  add_commands();
  cert_init();
  coro_context ctx_first;
  struct coro_stack stk;
  coro_create(&ctx_first, NULL, NULL, NULL, 0);
  coro_stack_alloc(&stk, 4096);
  coro_create(&ctx_sheduler, run_sheduler, NULL, stk.sptr, stk.ssze);
  coro_transfer(&ctx_first, &ctx_sheduler);
  return 0;
}
