#ifndef CRACKURL_H
#define CRACKURL_H

#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
  char *scheme;
  char *host;
  int port; /* always set, can be 0 for unknown protocol */
  char *portstr; /* can be NULL, starts with ':' */
  char *path; /* can be NULL, starts with '/' */
  char *query; /* can be NULL, starts with '?' */
  char *fragment; /* can be NULL, starts with '#' */
  char *username; /* can be NULL */
  char *password; /* can be NULL even when username is not NULL */
} cracked_url_t;


/* can clear UNLY PROPERLY INITIALIZED *url! */
extern void cracked_url_clear (cracked_url_t *url);

/* will just fill *url with zeroes at start (not with cracked_url_clear()!) */
/* will return property initialized *url in any case */
/* will return 0 if urlstr is ok and <0 on error (and *url is empty then) */
extern int crack_url (cracked_url_t *url, const char *urlstr);


#ifdef __cplusplus
}
#endif
#endif
