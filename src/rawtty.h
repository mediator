/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef RAWTTY_H
#define RAWTTY_H


int is_rawtty (void);

/* <0: error; 0: ok */
int rawtty_off (void);
int rawtty_on (void);
int rawtty_set (int raw);

int rawtty_is_keyhit (void);
int rawtty_waitkey (int to_msec); /* returns boolean 'is_hit'; to_msec <0: infinite; =0: immediate return */

int rawtty_width (void);
int rawtty_height (void);


#endif
