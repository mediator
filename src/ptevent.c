typedef struct {
  /*WARNING! NO USER-SERVICEABLE PARTS INSIDE!*/
  pthread_mutex_t mutex;
  /*pthread_mutexattr_t attr;*/
  pthread_cond_t cond;
  int signaled;
  int autoreset;
} ptevent_t;


/******************************************************************************/
void ptevent_init (ptevent_t *evt, int signaled, int autoreset) {
  if (evt != NULL) {
    memset(evt, 0, sizeof(*evt));
    evt->signaled = signaled;
    evt->autoreset = autoreset;
    pthread_mutex_init(&evt->mutex, NULL);
    pthread_cond_init(&evt->cond, NULL);
  }
}


void ptevent_deinit (ptevent_t *evt) {
  if (evt != NULL) {
    pthread_cond_destroy(&evt->cond);
    pthread_mutex_destroy(&evt->mutex);
    memset(evt, 0, sizeof(*evt));
  }
}


void ptevent_set (ptevent_t *evt) {
  if (evt != NULL) {
    pthread_mutex_lock(&evt->mutex);
    // only signal if we are unsignalled yet
    if (!evt->signaled) {
      evt->signaled = 1;
      pthread_cond_signal(&evt->cond);
    }
    pthread_mutex_unlock(&evt->mutex);
  }
}


void ptevent_reset (ptevent_t *evt) {
  if (evt != NULL) {
    pthread_mutex_lock(&evt->mutex);
    evt->signaled = 0;
    pthread_mutex_unlock(&evt->mutex);
  }
}


int ptevent_wait_timeout (ptevent_t *evt, int timeout) {
  if (evt != NULL) {
    pthread_mutex_lock(&evt->mutex);
    if (timeout < 0) {
      while (!evt->signaled) pthread_cond_wait(&evt->cond, &evt->mutex);
    } else if (timeout > 0) {
      if (!evt->signaled) {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        if (timeout > 0) {
          long ns = ts.tv_nsec+((timeout%1000)*1000000);
          ts.tv_sec += timeout/1000+(ns/1000000000);
          ts.tv_nsec = ns%1000000000;
        }
        int rc = pthread_cond_timedwait(&evt->cond, &evt->mutex, &ts);
        if (rc != 0 /*== ETIMEDOUT*/ && !evt->signaled) {
          /*fprintf(stderr, "pthread_cond_timedwait(): rc=%d\n", rc);*/
          pthread_mutex_unlock(&evt->mutex);
          return -1;
        }
      }
    }
    if (evt->autoreset) evt->signaled = 0;
    pthread_mutex_unlock(&evt->mutex);
    return 0;
  }
  return -1;
}


static inline void ptevent_wait (ptevent_t *evt) { ptevent_wait_timeout(evt, -1); }
