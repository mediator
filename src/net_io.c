/**
 * Read data from socket.
 *
 * Params:
 *  fd = socket fd
 *  buf = destination buffer
 *  len = number of bytes to read
 *  flags = recv flags (pass MSG_WAITALL to read exact len bytes)
 *  to_mssec = read timeout
 *
 * Returns:
 *  either number of bytes read or <0 on error
 *  if there is no error, but errno set to ECONNRESET, connection was closed
 */
static int asnet_recv (int fd, void *buf, int len, int flags, int to_mssec) {
  int doall = (flags&MSG_WAITALL), total = 0;
  uint8_t *b = (uint8_t *)buf;
  //int rcnt = 0;
  flags &= ~MSG_WAITALL;
  if (len == 0) { errno = EINVAL; return -1; } // wtf?!
  for (;;) {
    ssize_t rd = recv(fd, b, len, flags);
    //ptlogf("asnet_recv: fd=%d; len=%d; flags=%d; to=%d; rd=%d; errno=%d\n", fd, len, flags, to_mssec, rd, errno);
    if (rd < 0) {
      if (errno == EINTR) {
        // interrupted by signal, try again
        ptlogf("asnet_recv: EINTR\n");
        continue;
      }
      if (errno == EAGAIN || errno == EWOULDBLOCK) {
        // operation will block, so do select() on this fd
        //ptlogf("asnet_recv: EAGAIN (%d)\n", fd);
        //ptlogf("cur_trd before: %p\n", cur_trd);
        coro_fd_add(fd, SAR_READ, to_mssec);
        // and pass control
        coro_yield();
        //ptlogf("cur_trd after: %p\n", cur_trd);
        // and do it again or return error
        if (coro_fd_check(fd)&SAR_READ) continue;
        // error
        ptlogf("asnet_recv: EAGAIN failed (%d)\n", fd);
        errno = ECONNRESET;
        return -1;
      }
      ptlogf("asnet_recv: some ERROR occured\n");
      return -1; // alas, some error occured
    }
    if (rd == 0) {
      ptlogf("asnet_recv: CLOSED (doall=%d)\n", doall);
      /*
      if (rcnt++ == 0) {
        coro_fd_add(fd, SAR_READ, to_mssec);
        // and pass control
        coro_yield();
        if (coro_fd_check(fd)&SAR_READ) continue;
      }
      */
      errno = ECONNRESET;
      if (doall) return -1; // alas
      return total;
    }
    total += rd;
    if (!doall) break;
    if ((len -= rd) == 0) break;
    b += rd;
  }
  errno = 0;
  return total;
}


// send it all!
// returns either 1 or <0
static int asnet_send (int fd, const void *buf, int len, int to_mssec) {
  const uint8_t *b = (const uint8_t *)buf;
  if (len == 0) { errno = 0; return 0; } // wtf?!
  for (;;) {
    ssize_t wr = send(fd, b, len, MSG_NOSIGNAL);
    //ptlogf("\3net_send(fd=%d): len=%d; wr=%d\n", fd, len, wr);
    if (wr < 0) {
      if (errno == EINTR) continue; // interrupted by signal, try again
      if (errno == EAGAIN || errno == EWOULDBLOCK) {
        // operation will block, so do select() on this fd
        coro_fd_add(fd, SAR_WRITE, to_mssec);
        // and pass control
        coro_yield();
        // and do it again or return error
        if (coro_fd_check(fd)&SAR_WRITE) continue;
        //ptlogf("asnet_send(fd=%d): st=0x%02x\n", fd, coro_fd_check(fd));
        errno = ECONNRESET;
        return -1; // error
      } else {
        /*
        if (errno == EPIPE) {
          ptlogf("\2net_send(fd=%d): PIPE\n", fd);
          coro_fd_add(fd, SAR_WRITE, to_mssec);
          coro_yield();
          continue;
        }
        */
        //ptlogf("\2net_send(fd=%d): err=%d; error=[%s]\n", fd, errno, strerror(errno));
      }
      return -1; // alas, some error occured
    }
    if (wr == 0) {
      //ptlogf("asnet_send(fd=%d) closed: st=0x%02x\n", fd, coro_fd_check(fd));
      errno = ECONNRESET;
      return -1; // connection closed? can it be? anyway, return error
    }
    if ((len -= wr) == 0) break;
    b += wr;
  }
  errno = 0;
  return 1; // everything was sent ok
}


// returns -1 on error; 0 on gracefull close and 1 if there CAN be some data
/*
static inline int asnet_recv_check (int fd) {
  for (;;) {
    uint8_t tmp;
    int res = recv(fd, &tmp, 1, MSG_DONTWAIT|MSG_PEEK);
    if (res < 0) {
      if (errno == EINTR) continue;
      if (errno == EAGAIN || errno == EWOULDBLOCK) return 1; // there can be some data
      return -1; // error
    }
    errno = 0;
    return (res > 0 ? 1 : 0);
  }
}
*/


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  int fd;
  ssl_context *ssl;
  int is_client;
} sock_t;


static void sock_init_empty (sock_t *sk, int is_client) {
  memset(sk, 0, sizeof(*sk));
  sk->fd = -1;
  sk->is_client = is_client;
}


static void sock_init_with_fd (sock_t *sk, int fd, int is_client) {
  memset(sk, 0, sizeof(*sk));
  sk->fd = fd;
  sk->is_client = is_client;
}


static void sock_abort (sock_t *sk) {
  if (sk->ssl != NULL) {
    ssl_free(sk->ssl);
    free(sk->ssl);
    sk->ssl = NULL;
  }
  if (sk->fd >= 0) {
    skclose(sk->fd);
    sk->fd = -1;
  }
}


static void sock_close (sock_t *sk) {
  if (sk->fd >= 0 && sk->ssl != NULL) ssl_close_notify(sk->ssl);
  sock_abort(sk);
}


static inline int sock_is_alive (const sock_t *sk) {
  return (sk->fd >= 0);
}


static inline int sock_fd (const sock_t *sk) {
  return sk->fd;
}


static inline int sock_is_ssl (const sock_t *sk) {
  return (sk->ssl != NULL);
}


// do SSL handshake if necessary
static int sock_handshake (sock_t *sk) {
  if (sk->fd >= 0 && sk->ssl) {
    int res;
    ptlogf("SSL: performing handshake...\n");
    while ((res = ssl_handshake(sk->ssl)) != 0) {
      if (res != POLARSSL_ERR_NET_WANT_READ && res != POLARSSL_ERR_NET_WANT_WRITE) {
        ptlogf("FUCKED: ssl_handshake returned %d\n", res);
        return -1;
      }
    }
  }
  return 0;
}


/**
 * Read data from socket.
 *
 * Params:
 *  fd = socket fd
 *  buf = destination buffer
 *  len = number of bytes to read
 *  flags = recv flags (pass MSG_WAITALL to read exact len bytes)
 *
 * Returns:
 *  either number of bytes read or <0 on error
 *  if there is no error, but errno set to ECONNRESET, connection was closed
 */
static int sock_recv (sock_t *sk, void *buf, int len, int flags) {
  if (sk->fd < 0 || len < 1 || buf == NULL) { errno = EINVAL; return -1; } // alas
  if (sk->ssl == NULL) {
    // normal socket
    return asnet_recv(sk->fd, buf, len, flags, (sk->is_client ? TIMEOUT_CLIENT : TIMEOUT_SERVER));
  } else {
    // SSL socket, read and write callbacks MUST be initialised
    //ptlogf("*SSL sock_recv(fd=%d; flags=%d)\n", sk->fd, flags);
    if (flags&MSG_PEEK) { errno = EINVAL; return -1; } // alas, SSL cannot into 'peek'
    if ((flags&MSG_WAITALL) == 0) {
      int res = ssl_read(sk->ssl, buf, len);
      if (res == POLARSSL_ERR_NET_CONN_RESET) {
        errno = ECONNRESET;
        return 0;
      }
      return res;
    } else {
      // we want all data!
      uint8_t *b = (uint8_t *)buf;
      int total = 0;
      while (len > 0) {
        int rd = ssl_read(sk->ssl, b, len);
        if (rd <= 0) return -1; // alas
        b += rd;
        total += rd;
        len -= rd;
      }
      errno = 0;
      return total;
    }
  }
}


/**
 * Write data to socket. It always writes all data.
 *
 * Params:
 *  fd = socket fd
 *  buf = destination buffer
 *  len = number of bytes to read
 *
 * Returns:
 *  either number of bytes written (always len) or <0 on error
 */
static int sock_send (sock_t *sk, const void *buf, int len) {
  if (sk->fd < 0 || len < 1 || buf == NULL) return -1; // alas
  if (sk->ssl == NULL) {
    // normal socket
    if (asnet_send(sk->fd, buf, len, (sk->is_client ? TIMEOUT_CLIENT : TIMEOUT_SERVER)) < 0) return -1;
    return len;
  } else {
    // SSL socket, read and write callbacks MUST be initialised
    // we want all data!
    //ptlogf("*SSL sock_send(fd=%d)\n", sk->fd);
    const uint8_t *b = (const uint8_t *)buf;
    int total = 0;
    while (len > 0) {
      int rd = ssl_write(sk->ssl, b, len);
      if (rd <= 0) return -1; // alas
      b += rd;
      total += rd;
      len -= rd;
    }
    return total;
  }
}


////////////////////////////////////////////////////////////////////////////////
static inline int send_full (sock_t *sk, const void *buf, int len) {
  if (len < 0) len = strlen((const char *)buf);
  return (sock_send(sk, buf, len) < 0 ? -1 : 0);
}


////////////////////////////////////////////////////////////////////////////////
// <0: error; 0: connection closed; >0: ok
static inline int xbuf_read (sock_t *sk, xbuf_t *buf) {
  while (buf->used < buf->size) {
    int res = sock_recv(sk, buf->buf+buf->used, buf->size-buf->used, 0);
    //ptlogf("xbuf_read: res=%d; errno=%d", res, errno);
    if (res < 0) {
      ptlogf("WARNING: recv error worker thread (xbuf_read:fd=%d)!\n", sk->fd);
      return -1;
    }
    if (res == 0) return 0;
    buf->used += res;
    buf->total += res;
  }
  return 1;
}


// <0: error; 0: connection closed; >0: ok
static inline int xbuf_write (sock_t *sk, xbuf_t *buf) {
  while (buf->used > 0) {
    int res = sock_send(sk, buf->buf, buf->used);
    if (res < 0) {
      ptlogf("WARNING: send error in worker thread (xbuf_write:fd=%d)!\n", sk->fd);
      return -1;
    }
    if (res == 0) return 0;
    buf->used -= res;
    if (buf->used > 0) memmove(buf->buf, buf->buf+res, buf->used);
  }
  return 1;
}
