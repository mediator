static int cmdline_active = 0;
static char cmdline_input[1024];
static int cmdline_input_len = 0;
static int cmdline_curpos = 2;

static char *cmdline_argv[64] = {0};
static int cmdline_argc = 0;


static void cmdline_beep (void) {
  if (cmdline_active > 0) write(STDOUT_FILENO, "\x07", 1);
}


__attribute__((destructor)) static void dtor_cmline_ (void) {
  for (int f = 0; f < ARRAYLEN(cmdline_argv); ++f) {
    if (cmdline_argv[f] != NULL) free(cmdline_argv[f]);
    cmdline_argv[f] = NULL;
  }
}


static void cmdline_parse_command_line (void) {
  int pos = 0;
  cmdline_argc = 0;
  cmdline_input[cmdline_input_len] = 0;
  for (int f = 0; f < ARRAYLEN(cmdline_argv); ++f) {
    if (cmdline_argv[f] != NULL) free(cmdline_argv[f]);
    cmdline_argv[f] = NULL;
  }
  while (pos < cmdline_input_len) {
    // skip spaces
    const char *sp;
    while (pos < cmdline_input_len && isspace(cmdline_input[pos])) ++pos;
    if (pos >= cmdline_input_len) break;
    if (cmdline_argc >= ARRAYLEN(cmdline_argv)) break;
    // save starting pos
    sp = cmdline_input+pos;
    // skip non-spaces
    while (pos < cmdline_input_len && !isspace(cmdline_input[pos])) ++pos;
    // mark end and skip it
    cmdline_input[pos++] = 0;
    cmdline_argv[cmdline_argc++] = strdup(sp);
  }
  cmdline_input[0] = 0;
  cmdline_input_len = 0;
}


__attribute__((format(printf,1,2))) static void cmdline_addf (const char *fmt, ...) {
  static char str[65536];
  va_list ap;
  int cnt;
  va_start(ap, fmt);
  cnt = vsnprintf(str, sizeof(str), fmt, ap);
  va_end(ap);
  if (cmdline_input_len+cnt >= sizeof(cmdline_input)) {
    cmdline_beep();
  } else {
    cmdline_input[cmdline_input_len] = 0;
    strcat(cmdline_input, str);
    cmdline_input_len = strlen(cmdline_input);
  }
}


typedef void (*cmdline_cb) (int argc, char *argv[], int do_autocomplete);


typedef struct {
  char *name;
  char *desc;
  cmdline_cb handler;
} cmdline_command_t;

static cmdline_command_t *cmdline_command_list = NULL;
static int cmdline_command_count = 0;


#define CMDLINE_CMD(name,desc) \
static void cmdline_cmd_##name##_ (int argc, char *argv[], int do_autocomplete); \
__attribute__((constructor)) static void ctor_cmd_##name##_ (void) { \
  cmdline_add_command(#name, cmdline_cmd_##name##_, desc); \
} \
static void cmdline_cmd_##name##_ (int argc, char *argv[], int do_autocomplete)


#define CMDLINE_DEFAULT_AC() \
if (do_autocomplete) { \
  for (int f = 0; f < argc; ++f) cmdline_addf("%s ", argv[f]); \
  return; \
}


static int cmdline_find_command (const char *name, int namelen) {
  if (name != NULL) {
    if (namelen < 0) namelen = strlen(name);
    for (int f = 0; f < cmdline_command_count; ++f) {
      if (strncmp(cmdline_command_list[f].name, name, namelen) == 0 && strlen(cmdline_command_list[f].name) == namelen) return f;
    }
  }
  return -1;
}


static int cmdline_sort_cmp (const void *ip0, const void *ip1) {
  const cmdline_command_t *i0 = (const cmdline_command_t *)ip0;
  const cmdline_command_t *i1 = (const cmdline_command_t *)ip1;
  return strcmp(i0->name, i1->name);
}


static void cmdline_add_command (const char *name, cmdline_cb handler, const char *desc) {
  if (name != NULL) {
    int idx = cmdline_find_command(name, -1);
    if (idx < 0 && handler == NULL) return; // nothing to do
    if (desc == NULL) desc = "";
    if (idx >= 0) {
      if (handler == NULL) {
        // remove command
        abort(); // not yet
      } else {
        // replace command
        cmdline_command_list[idx].handler = handler;
        free(cmdline_command_list[idx].desc);
        cmdline_command_list[idx].desc = strdup(desc);
      }
    } else {
      // new command
      void *np = realloc(cmdline_command_list, sizeof(cmdline_command_list[0])*(cmdline_command_count+1));
      if (np == NULL) abort();
      cmdline_command_list = np;
      idx = cmdline_command_count++;
      cmdline_command_list[idx].name = strdup(name);
      cmdline_command_list[idx].desc = strdup(desc);
      cmdline_command_list[idx].handler = handler;
      qsort(cmdline_command_list, cmdline_command_count, sizeof(cmdline_command_list[0]), cmdline_sort_cmp);
    }
  }
}


static void cmdline_process_command (void) {
  cmdline_parse_command_line();
  if (cmdline_argc > 0) {
    int idx;
    if (strcmp(cmdline_argv[0], "help") == 0 && cmdline_argc == 2) {
      idx = cmdline_find_command(cmdline_argv[1], -1);
      if (idx >= 0) {
        if (cmdline_command_list[idx].desc[0]) {
          conlogf("%s: %s\n", cmdline_command_list[idx].name, cmdline_command_list[idx].desc);
        } else {
          conlogf("%s: no help\n", cmdline_command_list[idx].name);
        }
        return;
      }
    }
    idx = cmdline_find_command(cmdline_argv[0], -1);
    if (idx >= 0) {
      cmdline_command_list[idx].handler(cmdline_argc, cmdline_argv, 0);
    } else {
      conlogf("unknown command: '%s'\n", cmdline_argv[0]);
    }
  }
}


static void cmdline_autocomplete (void) {
  int pos = 0;
  if (cmdline_input_len == 0 && cmdline_active > 0) {
    // �������� ���
    for (int idx = 0; idx < cmdline_command_count; ++idx) conlogf(" %s\n", cmdline_command_list[idx].name);
    return;
  }
  if (cmdline_input_len >= ARRAYLEN(cmdline_input)-4) return;
  if (cmdline_input_len == 0 && isspace(cmdline_input[0])) return;
  while (pos < cmdline_input_len && !isspace(cmdline_input[pos])) ++pos;
  if (pos < cmdline_input_len) {
    // this is autocomplete query for command
    int idx = cmdline_find_command(cmdline_input, pos);
    if (idx >= 0) {
      cmdline_parse_command_line();
      cmdline_command_list[idx].handler(cmdline_argc, cmdline_argv, 1);
    } else {
      cmdline_beep();
    }
    return;
  }
  cmdline_input[pos] = 0;
  {
    const char *found = NULL;
    int foundlen = 0;
    int pfxcount = 0;
    int slen = pos;
    // ������ ������: ������� ��������, ���������� ����� ������� ��������
    for (int idx = 0; idx < cmdline_command_count; ++idx) {
      const char *s = cmdline_command_list[idx].name;
      int l0 = strlen(s);
      if (slen <= l0 && strncmp(s, cmdline_input, slen) == 0) {
        // �����
        if (l0 > foundlen) {
          found = s;
          foundlen = l0;
        }
        ++pfxcount;
      }
    }
    if (pfxcount == 0) { cmdline_beep(); return; } // �� ����� ������ ������, ����� ������, ������!
    if (pfxcount == 1) {
      // ����� ����, �������!
      cmdline_input_len = snprintf(cmdline_input, sizeof(cmdline_input), "%s ", found);
      return;
    }
    // ����� ������, ��� ����������
    // ���� ����� ������� �������, ������ �������� �ӣ, ��� �����
    snprintf(cmdline_input, sizeof(cmdline_input), "%s", found); // ������� �� ������, � ���������� ���
    for (int idx = 0; idx < cmdline_command_count; ++idx) {
      const char *s = cmdline_command_list[idx].name;
      if (strncmp(s, cmdline_input, slen) == 0) {
        // ����, �������� � ������ �������
        char *t = cmdline_input;
        conlogf("%s\n", s);
        while (*t && *s && *t == *s) { ++t; ++s; }
        *t = '\0'; // �� ������, ��� � �������
        int l = strlen(cmdline_input);
        if (l < slen) slen = l;
      }
    }
    cmdline_input_len = strlen(cmdline_input);
    // �ӣ
  }
}


static void cmdline_init (void) {
  if (!cmdline_active) {
    if (isatty(STDIN_FILENO) && isatty(STDOUT_FILENO)) {
      cmdline_active = 1;
      rawtty_on();
      cmdline_input_len = 0;
      cmdline_curpos = 2;
    } else {
      cmdline_active = -11;
    }
  }
}


static void cmdline_stop (void) {
  if (cmdline_active > 0) {
    rawtty_off();
    cmdline_active = 0;
  }
  for (int f = cmdline_command_count-1; f >= 0; --f) {
    free(cmdline_command_list[f].desc);
    free(cmdline_command_list[f].name);
  }
  if (cmdline_command_list != NULL) free(cmdline_command_list);
  cmdline_command_list = NULL;
  cmdline_command_count = 0;
}


static int cmdline_fd (void) {
  return (cmdline_active > 0 ? STDIN_FILENO : -1);
}


static void cmdline_setcur (void) {
  if (cmdline_active > 0) {
    char buf[64];
    int len = snprintf(buf, sizeof(buf), "\x1b[1;%dH", cmdline_curpos);
    write(STDOUT_FILENO, buf, len);
  }
}


static void cmdline_draw (void) {
  if (cmdline_active > 0) {
    int stpos = 0, len = cmdline_input_len;
    int wdt = rawtty_width()-2;
    //write(STDOUT_FILENO, "\x1b[1;1H\x1b[0;44;33;2m>", 19);
    write(STDOUT_FILENO, "\x1b[1;1H\x1b[0m>", 11);
    if (len > wdt) {
      stpos += len-wdt;
      len = wdt;
    }
    if (len > 0) write(STDOUT_FILENO, cmdline_input+stpos, len);
    write(STDOUT_FILENO, "\x1b[K\x1b[0m", 7);
  }
}


static void cmdline_fix_curpos (void) {
  if (cmdline_active > 0) {
    int wdt = rawtty_width();
    int ll = cmdline_input_len+2;
    if (ll > wdt) ll = wdt;
    cmdline_curpos = ll;
  }
}


// no need to check for cmdline_active here
static void cmdline_process_keys (void) {
  if (cmdline_active <= 0) return; // just in case
  while (rawtty_is_keyhit()) {
    uint8_t ch;
    if (read(STDIN_FILENO, &ch, 1) != 1) break;
    if (ch == 3) {
      // ^C
      raise(SIGINT);
    } else if (ch == 8 || ch == 127) {
      // BS
      if (cmdline_input_len > 0) --cmdline_input_len;
    } else if (ch == 9) {
      // tab
      cmdline_autocomplete();
    } else if (ch == 10) {
      // enter
      //cmdline_input[cmdline_input_len] = 0;
      //cmdline_input_len = 0;
      cmdline_process_command();
    } else if (ch == 25) {
      // ^Y
      cmdline_input_len = 0;
    } else if (ch >= 32 && ch < 127) {
      if (cmdline_input_len+2 < sizeof(cmdline_input)) {
        cmdline_input[cmdline_input_len++] = ch;
      } else {
        cmdline_beep();
      }
    }
  }
  cmdline_fix_curpos();
  cmdline_draw();
}


/*
 * ���������� �� ������, ������� ��������
 * ���� � ����� 0: �����, ������, �������, ����, ������, ����������, ���������� ""
 * ���� � ����� 1: �������, ���������� ���� 1
 * ���� � ����� �����: �������� �������� ������� �������, ����������; ������ �������� ��� ��������� �������
 * ��������� �� ������ NULL, � ��� ���� free()
 * ����! `cmds` ������ ���� �������� �������, ��� ������ ��������
 * ���� `cmds` -- NULL, �� ���������� �ӣ
 * `matches` ��������������� � ���������� �������� ��������� (�.�. 0 -- ���, 1 -- �����, � �� ��)
 */
__attribute__((sentinel)) static char *cmdline_complete_abbrev (const char *cmds, int doprint, int *matches, ...) {
  if (matches != NULL) *matches = 0;
  if (cmds != NULL && cmds[0]) {
    va_list va;
    const char *found = NULL, *s;
    int foundlen = 0;
    int pfxcount = 0;
    int slen = strlen(cmds);
    char *res;
    // ������ ������: ������� ��������, ���������� ����� ������� ��������
    va_start(va, matches);
    while ((s = va_arg(va, const char *)) != NULL) {
      int l0 = strlen(s);
      if (slen <= l0 && strncmp(s, cmds, slen) == 0) {
        // �����
        if (l0 > foundlen) {
          found = s;
          foundlen = l0;
        }
        ++pfxcount;
      }
    }
    va_end(va);
    if (matches != NULL) *matches = pfxcount;
    if (pfxcount == 0) return strdup(cmds); // �� ����� ������ ������, ����� ������, ������!
    if (pfxcount == 1) return strdup(found); // ����� ����� ����, ��� ���, �������!
    // ����� ������, ��� ����������
    // ���� ����� ������� �������, ������ �������� �ӣ, ��� �����
    res = strdup(found); // ������� �� ������, � ���������� ���
    va_start(va, matches);
    while ((s = va_arg(va, const char *)) != NULL) {
      if (strncmp(s, res, slen) == 0) {
        // ����, �������� � ������ �������
        char *t = res;
        if (doprint) conlogf(" %s\n", s);
        while (*t && *s && *t == *s) { ++t; ++s; }
        *t = '\0'; // �� ������, ��� � �������
      }
    }
    va_end(va);
    // ���, ���������� ��������
    return res;
  } else if (cmds == NULL && doprint) {
    // ���� ���������� ��� ���������
    va_list va;
    const char *s;
    va_start(va, matches);
    while ((s = va_arg(va, const char *)) != NULL) conlogf(" %s\n", s);
    va_end(va);
  }
  return strdup("");
}
