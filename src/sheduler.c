#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>


#define MAX_WORKER_THREADS  (256)

//#define sdbg_ptlogf  ptlogf
#ifndef sdbg_ptlogf
# define sdbg_ptlogf(...)  ((void)0)
#endif

//#define PFD_RDHUP  POLLRDHUP
#ifndef PFD_RDHUP
# define PFD_RDHUP  (0)
#endif


static struct pollfd *shed_pfd = NULL; // array
static int shed_pfd_max = 0; // [0..shed_pfd_max) fds in shed_pfd are allowed
static int shed_wait_milli = -1; // <=0: forever


// get fd flags (if any)
static inline int coro_fd_check (int fd) {
  if (fd >= 0) for (int f = 0; f < MAX_FDWAITS; ++f) if (cur_trd->wres[f].fd == fd) return cur_trd->wres[f].flags;
  return 0;
}


// flags: SAR_READ, SAR_WRITE
static void pfd_add (int fd, int flags, int to_milli) {
  if (fd >= 0) {
    if (fd >= shed_pfd_max) {
      // need more spice^w room
      shed_pfd = realloc(shed_pfd, (fd+1)*sizeof(shed_pfd[0]));
      if (shed_pfd == NULL) {
        ptlogf("FATAL: out of memory!\n");
        abort();
      }
      sdbg_ptlogf("PFD GROW from %d to %d\n", shed_pfd_max, fd+1);
      // ignore added fds
      while (shed_pfd_max < fd+1) {
        shed_pfd[shed_pfd_max].fd = -1;
        shed_pfd[shed_pfd_max].events = PFD_RDHUP; // connection reset; this will set SAR_TIMEOUT flag for now
        shed_pfd[shed_pfd_max].revents = 0;
        ++shed_pfd_max;
      }
    }
    sdbg_ptlogf("PFD ADD: fd=%d; flags=0x%02x; to=%d\n", fd, flags, to_milli);
    shed_pfd[fd].fd = fd;
    if (flags&SAR_READ) shed_pfd[fd].events |= POLLIN;
    if (flags&SAR_WRITE) shed_pfd[fd].events |= POLLOUT;
    // timeout check
    if (to_milli > 0 && (shed_wait_milli < 0 || to_milli < shed_wait_milli)) shed_wait_milli = to_milli;
  }
}


// this will REPLACE previous flags and timeout for the given fd!
static void coro_fd_add (int fd, fd_flags flags, int to_milli) {
  if (cur_trd->clifd >= 0 && fd >= 0) {
    int free = -1;
    flags &= SAR_READ|SAR_WRITE;
    if (flags == 0) {
      ptlogf("FATAL: waiting for nothing!\n");
      abort();
    }
    // add to pfd set
    pfd_add(fd, flags, to_milli);
    // add this fd to coro set
    for (int f = 0; f < MAX_FDWAITS; ++f) {
      if (cur_trd->waits[f].fd == fd) {
        flags |= cur_trd->waits[f].flags;
        free = f;
        break;
      } else if (free < 0 && cur_trd->waits[f].fd < 0) {
        free = f;
      }
    }
    if (free < 0) {
      ptlogf("FATAL: too many fds for waiting!\n");
      abort();
    }
    cur_trd->waits[free].fd = fd;
    cur_trd->waits[free].flags = flags;
    cur_trd->waits[free].expire_time_milli = (to_milli > 0 ? k8clock()+to_milli : 0);
    cur_trd->sleep_expire_time_milli = 0;
  }
}


// this will REPLACE previous flags and timeout for the given fd!
static void coro_sleep (int to_milli) {
  if (cur_trd->clifd >= 0 && to_milli >= 0) {
    // clear fds
    for (int f = 0; f < MAX_FDWAITS; ++f) cur_trd->waits[f].fd = -1;
    if (to_milli < 0) to_milli = 1;
    // fix shed_wait
    if (shed_wait_milli < 0 || to_milli < shed_wait_milli) shed_wait_milli = to_milli;
    // set wait timeout
    cur_trd->sleep_expire_time_milli = k8clock()+to_milli;
    // pass control
    coro_yield();
  }
}


//WARNING! should be called from sheduler!
static void sk_process_set (int do_resolv) {
  uint64_t ctt = k8clock();
  // first fill all wres sets and reset pfd
  for (coro_info_t *trd = coro_list; trd != NULL; trd = trd->next) {
    if (trd->clifd < 0) continue;
    trd->rc_state = 0;
    if (trd->resolving_name == 0 && trd->sleep_expire_time_milli == 0) {
      int was_hit = 0, have_refill = 0;
      for (int f = 0; f < MAX_FDWAITS; ++f) {
        int fd = trd->waits[f].fd;
        if (fd >= 0) {
          int new_flags = SAR_NONE;
          if (shed_pfd[fd].revents&(POLLERR|POLLHUP|PFD_RDHUP)) {
            new_flags |= SAR_TIMEOUT; // error, signal as timeout for now
            if (shed_pfd[fd].revents&POLLERR) _ptlog(trd, "\2fd=%d; POLLERR", fd);
            if (shed_pfd[fd].revents&POLLHUP) _ptlog(trd, "\2fd=%d; POLLHUP", fd);
            if (shed_pfd[fd].revents&POLLRDHUP) _ptlog(trd, "\2fd=%d; POLLRDHUP", fd);
          }
          if (shed_pfd[fd].revents&POLLIN) new_flags |= SAR_READ;
          if (shed_pfd[fd].revents&POLLOUT) new_flags |= SAR_WRITE;
          if (new_flags == SAR_NONE && trd->waits[f].expire_time_milli > 0 && ctt >= trd->waits[f].expire_time_milli) new_flags |= SAR_TIMEOUT;
          if (new_flags != SAR_NONE) {
            // hit; update wres set
            //_ptlog(trd, "\3fd=%d; sr=0x%02x\n", fd, new_flags);
            trd->wres[f].fd = fd;
            trd->wres[f].flags = new_flags;
            trd->wres[f].expire_time_milli = trd->waits[f].expire_time_milli;
            // remove this fd from 'refill' set
            trd->waits[f].fd = -1;
            was_hit = 1;
          } else {
            trd->wres[f].fd = -1;
            trd->wres[f].flags = 0;
            trd->wres[f].expire_time_milli = 0;
            have_refill = 1;
          }
        }
      }
      // if there was some event, don't refill this thread's set
      if (was_hit) {
        trd->rc_state = 1; // was event, need to shedule
        // clear 'waits'
        if (have_refill) for (int f = 0; f < MAX_FDWAITS; ++f) trd->waits[f].fd = -1;
      } else if (have_refill) {
        trd->rc_state = -1; // was no event, need to refill
      } else {
        _ptlog(trd, "\2WTF?! thread without FDs and not in 'resolving' state!");
      }
    } else {
      // either resolving or sleeping; only timeout check
      for (int f = 0; f < MAX_FDWAITS; ++f) {
        trd->waits[f].fd = -1;
        trd->waits[f].flags = 0;
        trd->waits[f].expire_time_milli = 0;
        //
        trd->wres[f].fd = -1;
        trd->wres[f].flags = 0;
        trd->wres[f].expire_time_milli = 0;
      }
    }
  }
  // now clean pfd...
  for (int f = 0; f < shed_pfd_max; ++f) {
    shed_pfd[f].fd = -1;
    shed_pfd[f].events = PFD_RDHUP; // connection reset; this will set SAR_TIMEOUT flag for now
    shed_pfd[f].revents = 0;
  }
  shed_wait_milli = -1;
  // and fill it with 'refill' sets
  for (coro_info_t *trd = coro_list; trd != NULL; trd = trd->next) {
    if (trd->clifd < 0 || trd->resolving_name != 0 || trd->rc_state >= 0) continue;
    //_ptlog(trd, "\3REFILL!");
    for (int f = 0; f < MAX_FDWAITS; ++f) {
      int fd = trd->waits[f].fd;
      if (fd >= 0) pfd_add(fd, trd->waits[f].flags, trd->waits[f].expire_time_milli > ctt ? trd->waits[f].expire_time_milli-ctt : 1);
    }
  }
  // process threads, shedule events
  for (coro_info_t *trd = coro_list, *next = NULL; trd != NULL; trd = next) {
    next = trd->next; // save it here 'cause thread can commit suicide
    if (trd->clifd < 0) continue;
    if (trd->resolving_name != 0) {
      // resolving
      if (trd->resolving_name > 0) {
        if (!do_resolv) continue;
        trd->resolving_name = gai_error(&trd->gacb);
        if (trd->resolving_name == EAI_INPROGRESS) {
          trd->resolving_name = 1;
          continue;
        }
      }
      // done, call corresponding thread
      //_ptlog(trd, "GOT NAME!");
      cur_trd = trd;
      trd->rc_state = 0;
      coro_transfer(&ctx_sheduler, &trd->ctx);
    } else if (trd->sleep_expire_time_milli > 0) {
      // sleeping
      coro_info_t *ot = cur_trd;
      cur_trd = trd;
      if (trd->sleep_expire_time_milli <= ctt) {
        // wake up!
        trd->sleep_expire_time_milli = 0;
        trd->rc_state = 0;
        coro_transfer(&ctx_sheduler, &trd->ctx);
      } else {
        // sleep tight
        coro_sleep(ctt-trd->sleep_expire_time_milli);
        cur_trd = ot;
      }
    } else if (trd->rc_state > 0) {
      // process 'result' set
      // some condition was satisfied, execute thread
      //_ptlog(trd, "event occured!");
      cur_trd = trd;
      trd->rc_state = 0;
      coro_transfer(&ctx_sheduler, &trd->ctx);
    } else if (trd->rc_state == 0) {
      _ptlog(trd, "FATAL: rc_state == 0!");
      abort();
    } else if (trd->rc_state < 0) {
      trd->rc_state = 0;
      //_ptlog(trd, "refilled!");
    }
  }
}


static void load_lists (void) {
  ptlogf("loading list of blocked hosts...");
  hp_load(&host_blocks, "data/blocked_hosts.rc");
  ptlogf("loading list of hosts with allowed referers...");
  hp_load(&host_allowrefs, "data/referer_hosts.rc");
  ptlogf("loading list of blocked URIs...");
  hp_load_re(&host_blockuris, "data/blocked_uris.rc");
  ptlogf("loading list of iframe-allowed URIs...");
  hp_load_re(&host_iframeuris, "data/iframe_uris.rc");
  ptlogf("loading list of UA replacemets...");
  relist_load_ua(&re_uareplace, "data/useragents.rc");
  ptlogf("loading list of IPv4-only hosts...");
  hp_load(&host_ipv4, "data/ipv4_hosts.rc");
  ptlogf("loading list of IPv6-only hosts...");
  hp_load(&host_ipv6, "data/ipv6_hosts.rc");
  ptlogf("loading list of HTTPS-only hosts...");
  hp_load(&host_force_https, "data/https_hosts.rc");
  ptlogf("loading list of URI rewrites...");
  rewrite_list_load(&re_rewrites, "data/rewrite.rc");
  ptlogf("loading list of Hyperboria hosts...");
  hypehosts_load("data/hyperhosts.rc");
  hp_dump(&host_blocks);
}


static void run_sheduler (void *arg) {
  int listenerfd, ressigfd, csigfd, cmdlinefd;
  // setup signal handlers
  csigfd = -1;
  ressigfd = -1;
  {
    /*
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_sigaction = sighandler;
    act.sa_flags = SA_SIGINFO;
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGHUP, &act, NULL);
    sigaction(SIGQUIT, &act, NULL);
    sigaction(SIGINT, &act, NULL);
    sigaction(SIGUSR1, &act, NULL);
    act.sa_sigaction = sigusr2handler;
    sigaction(SIGUSR2, &act, NULL);
    */
    //
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGTERM);
    sigaddset(&mask, SIGHUP);
    sigaddset(&mask, SIGQUIT);
    sigaddset(&mask, SIGINT);
    //sigprocmask(SIG_BLOCK, &mask, NULL); //we block the signals
    pthread_sigmask(SIG_BLOCK, &mask, NULL); //we block the signals
    csigfd = signalfd(-1, &mask, SFD_NONBLOCK);
  }
  ressigfd = setup_resolver_signalfd();
  //
  cmdline_init();
  cmdlinefd = cmdline_fd();
  //
  log_start();
  //
  cmdline_draw();
  //
  listenerfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (listenerfd == -1) {
    ptlogf("FATAL: can't create listening socket!\n");
    goto quit;
  }
  //
  {
    int opt = 1;
    if (setsockopt(listenerfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) == -1) {
      ptlogf("FATAL: failed to set SOREUSEADDR on socket\n");
      goto quit;
    }
  }
  //
  {
    struct sockaddr_in lsin;
    lsin.sin_family = PF_INET;
    lsin.sin_addr.s_addr = inet_addr("127.0.0.1"); //INADDR_ANY
    lsin.sin_port = htons(opt_proxy_port);
    if (bind(listenerfd, (struct sockaddr *)&lsin, sizeof(lsin)) == -1) {
      ptlogf("FATAL: failed to bind listening socket!\n");
      goto quit;
    }
  }
  socket_set_nonblocking(listenerfd);
  listen(listenerfd, SOMAXCONN);
  //socket_set_timeouts(listenerfd, TIMEOUT_CLIENT);
  {
    int v = 1;
    setsockopt(listenerfd, IPPROTO_TCP, TCP_DEFER_ACCEPT, &v, sizeof(v));
  }
  //
  ptlogf("listening on port %d\n", opt_proxy_port);
  ptlogf("starting sheduler...\n");
  //
  load_lists();
  //
  for (;;) {
    int do_accept, do_resolv, sr;
    static int prev_was_noto = 0; // previous state was 'no timeout'?
    //
    sdbg_ptlogf("*** LOOP ***");
    if (listenerfd >= 0) pfd_add(listenerfd, SAR_READ, 0); // this will be available if new connection comes
    if (ressigfd >= 0) pfd_add(ressigfd, SAR_READ, 0); // this will be available if resolver did something
    if (csigfd >= 0) pfd_add(csigfd, SAR_READ, 0); // this will be available if ^C pressed
    if (cmdlinefd >= 0) pfd_add(cmdlinefd, SAR_READ, 0); // this will be available when the key is pressed
    // determine minimal timeout
    if (shed_wait_milli <= 0) {
      // no timeouts in workers -- don't eat CPU
      shed_wait_milli = -1;
      if (!prev_was_noto) {
        ptlogf("NO TIMEOUT!\n");
        prev_was_noto = 1;
      }
    } else {
      prev_was_noto = 0;
    }
    //ptlogf("workers=%d; nfd=%d; to=%d\n", coro_count, nfd, (tp != NULL ? (int)(tp->tv_sec*1000+tp->tv_usec/1000) : 0));
    cmdline_setcur(); // put cursor to command line
    for (;;) {
      sr = poll(shed_pfd, shed_pfd_max, shed_wait_milli);
      //ptlogf("POLL RESULT: sr=%d\n", sr);
      if (sr < 0) {
        if (errno != EINTR) {
          // NOT interrupted by signal
          ptlogf("FATAL: POLL RESULT: sr=%d\n", sr);
          goto quit;
        }
        ptlogf("\2POLL INTERRUPTED BY SIGNAL!\n");
      }
      break; // either timeout or event
    }
    //
    if (csigfd >= 0 && (shed_pfd[csigfd].revents&POLLIN)) {
      // break signal(s) here
      sdbg_ptlogf("\1***SIGNAL: BREAK");
      struct signalfd_siginfo si;
      while (read(csigfd, &si, sizeof(si)) > 0) ++do_shutdown; // ignore errors here
      sdbg_ptlogf("GOT CSIGNAL!");
      if (do_shutdown >= 3) { ptlogf("USER ABORT!\n"); break; } // abort
      if (coro_count < 1) break;
    }
    // process hits
    if (ressigfd >= 0 && (shed_pfd[ressigfd].revents&POLLIN)) {
      sdbg_ptlogf("\1***SIGNAL: RESOLVER");
      struct signalfd_siginfo si;
      while (read(ressigfd, &si, sizeof(si)) > 0) ; // ignore errors here
      sdbg_ptlogf("GOT RSIGNAL!");
      do_resolv = 1;
    } else {
      do_resolv = 0;
    }
    // process keys
    if (cmdlinefd >= 0 && (shed_pfd[cmdlinefd].revents&POLLIN)) {
      cmdline_process_keys();
    }
    // reload lists if necessary
    if (do_hp_reload) {
      do_hp_reload = 0;
      load_lists();
    }
    do_accept = (listenerfd >= 0 && (shed_pfd[listenerfd].revents&POLLIN));
    // process coro events
    sdbg_ptlogf("processing set: started");
    sk_process_set(do_resolv);
    sdbg_ptlogf("processing set: finished");
    // check for new connection
    if (do_accept) {
      // hey, we have new connection!
      sdbg_ptlogf("\1***ACCEPTING");
      struct sockaddr_in peer_addr;
      socklen_t addrlen = sizeof(peer_addr);
      int clifd = accept4(listenerfd, (struct sockaddr *)&peer_addr, &addrlen, SOCK_NONBLOCK|SOCK_CLOEXEC);
      if (clifd >= 0) {
        //ptlogf("new connection; clifd=%d\n", clifd);
        if (do_shutdown || coro_count > MAX_WORKER_THREADS) {
          // too many workers: just drop this connection
          ptlogf("too many workers; dropping fd=%d\n", clifd);
          skabort(clifd);
        } else {
          coro_info_t *trd = coro_new(proxy_thread);
          trd->clifd = clifd;
          // call new thread right now, set processor knows nothing about it yet
          cur_trd = trd;
          coro_transfer(&ctx_sheduler, &trd->ctx);
          //ptlogf("returned from new worker");
        }
      } else {
        ptlogf("WARNING: failed to accept connection!\n");
      }
    }
    if (do_shutdown) {
      if (listenerfd >= 0) { skabort(listenerfd); listenerfd = -1; }
      if (coro_count < 1) break;
    }
  }
  ptlogf("SHUTDOWN!\n");
quit:
  //coro_close_all();
  cmdline_stop();
  skabort(listenerfd); // negative fd is OK
  log_stop();
  if (logfd >= 0) { close(logfd); logfd = -1; }
  if (shed_pfd != NULL) free(shed_pfd);
  exit(0);
}
