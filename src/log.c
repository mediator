////////////////////////////////////////////////////////////////////////////////
#include "ptevent.c"


////////////////////////////////////////////////////////////////////////////////
static char log_name[1024] = "zlog.log";
static int opt_log_disabled = 0;
static int opt_logtty_disabled = 0;
static int opt_logfile_disabled = 0;

static int logfd = -1;

static char log_buffer[2][65536];
static int log_buffer_used[2] = {0, 0};
static int log_buffer_current = 0;
static int log_buffer_to_write = 0;
static int log_stop_flag = 0; // >0: need to flush and stop; <0: all done


static pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;

static inline void log_lock (void) { pthread_mutex_lock(&log_mutex); }
static inline void log_unlock (void) { pthread_mutex_unlock(&log_mutex); }


static ptevent_t log_event_has_work; // signal this to flush buffer in writing thread
static ptevent_t log_event_work_complete; // signal this when buffer written


//#define logdf(...)  fprintf(stderr, __VA_ARGS__)
#ifndef logdf
# define logdf(...)  ((void)0)
#endif


static void *log_buffer_write_thread (void *nothing_) {
  for (;;) {
    // wait for something to do
    logdf("write_thread: waiting for work signal...\n");
    ptevent_wait(&log_event_has_work);
    log_lock(); //LOCK0
    logdf("write_thread: got work signal; log_stop_flag=%d\n", log_stop_flag);
    if (log_stop_flag) {
      // shutdown; write all buffers
      logdf("write_thread: flushing buffers...\n");
      for (int f = 2; f > 0; --f) {
        if (log_buffer_used[log_buffer_to_write] > 0) {
          logdf("write_thread: writing buffer #%d (%d bytes)...\n", log_buffer_to_write, log_buffer_used[log_buffer_to_write]);
          if (logfd >= 0) write(logfd, log_buffer[log_buffer_to_write], log_buffer_used[log_buffer_to_write]);
          log_buffer_used[log_buffer_to_write] = 0;
        }
        log_buffer_to_write ^= 1;
      }
      logdf("write_thread: sending work-complete signal...\n");
      if (log_stop_flag != 666) {
        log_stop_flag = -1;
        ptevent_set(&log_event_work_complete);
        log_unlock();//UNLOCK0
        break;
      } else {
        log_stop_flag = 0;
        ptevent_set(&log_event_work_complete);
        log_unlock();//UNLOCK0
        continue;
      }
    }
    logdf("write_thread: writing buffer #%d (%d bytes)...\n", log_buffer_to_write, log_buffer_used[log_buffer_to_write]);
    if (log_buffer_used[log_buffer_to_write] > 0) {
      if (logfd >= 0) write(logfd, log_buffer[log_buffer_to_write], log_buffer_used[log_buffer_to_write]);
      log_buffer_used[log_buffer_to_write] = 0;
    }
    log_buffer_to_write ^= 1;
    logdf("write_thread: sending work-complete signal...\n");
    ptevent_set(&log_event_work_complete);
    log_unlock();//UNLOCK0
  }
  logdf("write_thread: exiting...\n");
  return NULL;
}


// log_lock() MUST NOT be called
static void log_add_to_buffer (const char *data, size_t data_len) {
  while (data_len > 0) {
    if (log_buffer_used[log_buffer_current] == sizeof(log_buffer[log_buffer_current])) {
      int wait_all = 0;
      // need new buffer
      logdf("log_add_to_buffer: need new buffer...\n");
      log_buffer_current ^= 1;
      log_lock(); //LOCK0
      if (log_buffer_used[log_buffer_current] && log_buffer_current == log_buffer_to_write) {
        logdf("log_add_to_buffer: second buffer is used too, need to wait\n");
        wait_all = 1;
      }
      log_unlock(); //UNLOCK0
      logdf("log_add_to_buffer: asking to do some work...\n");
      ptevent_set(&log_event_has_work);
      if (wait_all) {
        for (;;) {
          logdf("log_add_to_buffer: waiting for 'work complete' signal...\n");
          ptevent_wait(&log_event_work_complete);
          log_lock(); //LOCK1
          ptevent_reset(&log_event_work_complete);
          logdf("log_add_to_buffer: log_buffer_current=%d; log_buffer_to_write=%d; used=%d\n", log_buffer_current, log_buffer_to_write, log_buffer_used[log_buffer_to_write]);
          if (log_buffer_used[log_buffer_to_write] == 0) break;
          log_unlock(); //UNLOCK1
          logdf("log_add_to_buffer: asking to do some work...\n");
          ptevent_set(&log_event_has_work);
        }
        log_buffer_current = log_buffer_to_write;
        log_unlock(); //UNLOCK1
        logdf("log_add_to_buffer: using buffer #%d\n", log_buffer_current);
      }
      logdf("log_add_to_buffer: got new buffer\n");
      if (log_buffer_used[log_buffer_current] != 0) abort(); // the thing that should not be
      logdf("log_add_to_buffer: new buffer is ok\n");
    }
    int left = sizeof(log_buffer[log_buffer_current])-log_buffer_used[log_buffer_current];
    if (left == 0) abort(); // the thing that should not be
    if (left > data_len) left = data_len;
    memcpy(log_buffer[log_buffer_current]+log_buffer_used[log_buffer_current], data, left);
    log_buffer_used[log_buffer_current] += left;
    data += left;
    data_len -= left;
  }
}


static inline int is_log_enabled (void) { return (__sync_val_compare_and_swap(&opt_log_disabled, 0, 0) == 0); }
static inline int is_flog_enabled (void) { return (__sync_val_compare_and_swap(&opt_logfile_disabled, 0, 0) == 0); }

static inline void log_set_enabled (int v) {
  log_lock();
  opt_log_disabled = !!v;
  log_unlock();
}

static inline void flog_set_enabled (int v) {
  log_lock();
  opt_logfile_disabled = !!v;
  log_unlock();
}


static void log_new (void) {
  ptevent_wait(&log_event_work_complete);
  ptevent_reset(&log_event_work_complete);
  log_lock();
  logdf("log_new: asking to flush buffers...\n");
  log_stop_flag = 666;
  while (log_stop_flag != 0) {
    log_unlock();
    ptevent_set(&log_event_has_work);
    ptevent_wait(&log_event_work_complete);
    ptevent_reset(&log_event_work_complete);
    log_lock();
  }
  //logdf("log_new: done.\n");
  if (logfd >= 0) {
    char *newname = malloc(8192);
    int n = 0;
    close(logfd);
    for (;;) {
      sprintf(newname, "%s.%02d", log_name, n);
      if (access(newname, F_OK) != 0) break;
      ++n;
    }
    rename(log_name, newname);
    free(newname);
    logfd = open(log_name, O_WRONLY|O_CREAT|O_TRUNC, 0644);
  }
  ptevent_set(&log_event_work_complete); // we need this to avoid hangups
  log_unlock();
}


static void log_flush (void) {
  ptevent_wait(&log_event_work_complete);
  ptevent_reset(&log_event_work_complete);
  log_lock();
  log_stop_flag = 666;
  while (log_stop_flag != 0) {
    log_unlock();
    ptevent_set(&log_event_has_work);
    ptevent_wait(&log_event_work_complete);
    ptevent_reset(&log_event_work_complete);
    log_lock();
  }
  ptevent_set(&log_event_work_complete); // we need this to avoid hangups
  log_unlock();
}


static int log_screen_swapped = 0;


static void log_start (void) {
  pthread_t wt;
  logfd = open(log_name, O_WRONLY|O_CREAT|O_TRUNC, 0644);
  ptevent_init(&log_event_has_work, 0, 1); // non-signalled, autoreset
  ptevent_init(&log_event_work_complete, 1, 0); // signalled, no autoreset
  if (pthread_create(&wt, NULL, log_buffer_write_thread, NULL) != 0) {
    fprintf(stderr, "FATAL: can't create logging thread!\n");
    abort();
  }
  pthread_detach(wt);
  if (isatty(STDOUT_FILENO)) {
    char buf[128];
    snprintf(buf, sizeof(buf), "\x1b[0m\x1b[2J\x1b[2;%dr\x1b[2;1H\x1b[s\x1b[1;1H", rawtty_height());
    write(STDOUT_FILENO, buf, strlen(buf));
    log_screen_swapped = 1;
  }
}


static void log_stop (void) {
  ptevent_wait(&log_event_work_complete);
  ptevent_reset(&log_event_work_complete);
  log_lock();
  logdf("log_stop: asking to flush buffers...\n");
  log_stop_flag = 1;
  while (log_stop_flag >= 0) {
    log_unlock();
    ptevent_set(&log_event_has_work);
    ptevent_wait(&log_event_work_complete);
    ptevent_reset(&log_event_work_complete);
    log_lock();
  }
  log_unlock();
  //logdf("log_stop: done.\n");
  if (log_screen_swapped) {
    char buf[128];
    snprintf(buf, sizeof(buf), "\x1b[r\x1b[0m\x1b[999;1H\x1b[K");
    write(STDOUT_FILENO, buf, strlen(buf));
    log_screen_swapped = 0;
  }
}


// this is NOT thread-safe, but we have no system threads in our server, so it's ok
static __attribute__((format(printf,2,3))) void _ptlog (const coro_info_t *trd, const char *fmt, ...) {
  static const char *hi_strs[] = {
    "\x1b[37;1m", /* 1: white */
    "\x1b[31;1m", /* 2: red */
    "\x1b[32;1m", /* 3: green */
    "\x1b[33;1m", /* 4: yellow */
    "\x1b[34;1m", /* 5: light blue */
    "\x1b[35;1m", /* 6: magenta */
    "\x1b[36;1m", /* 7: cyan */
  };
  static const char *norm_str = "\x1b[0m";
  static int istty = -1;
  static char str[65536];
  va_list ap;
  int cnt, hi = 0;
  if (opt_log_disabled || (opt_logfile_disabled && opt_logtty_disabled)) return; // log is disabled
  if (__sync_val_compare_and_swap(&log_stop_flag, 0, 0) != 0) return; // no more logs
  if (fmt[0] >= '\1' && fmt[0] <= sizeof(hi_strs)) hi = *fmt++;
  va_start(ap, fmt);
  cnt = vsnprintf(str, sizeof(str), fmt, ap);
  va_end(ap);
  if (cnt >= 0) {
    static char *lastbuf = NULL, xbuf[512];
    static int lastbuf_size = 0;
    char *s2, *p2, *p = str;
    int lcount = 1, xblen, bsz;
    time_t tm = time(NULL);
    struct tm tt;
    localtime_r(&tm, &tt);
    snprintf(xbuf, sizeof(xbuf), "[%3d] %02d:%02d:%02d ", (trd != NULL ? trd->tid : 0), tt.tm_hour, tt.tm_min, tt.tm_sec);
    xblen = strlen(xbuf);
    if (cnt > 0 && str[cnt-1] == '\n') str[cnt-1] = 0;
    for (const char *t = str; *t; ++t) if (*t == '\n') ++lcount;
    bsz = cnt+lcount*xblen+lcount+8;
    if (bsz > lastbuf_size) {
      lastbuf = realloc(lastbuf, bsz);
      lastbuf_size = bsz;
    }
    p2 = s2 = lastbuf;
    for (;;) {
      char *e = strchr(p, '\n'), *xe;
      if (e == NULL) e = p+strlen(p);
      xe = e;
      if (e > p && e[-1] == '\r') --e;
      memmove(p2, xbuf, xblen);
      p2 += xblen;
      if (e > p) { memmove(p2, p, e-p); p2 += e-p; }
      *p2++ = '\n';
      if (*xe != '\n') break;
      p = xe+1;
    }
    //free(str);
    if (!opt_logtty_disabled) {
      if (hi > 0) {
        if (istty < 0) istty = isatty(1);
        if (istty > 0) write(1, hi_strs[hi-1], strlen(hi_strs[hi-1]));
      }
      if (istty) write(STDOUT_FILENO, "\x1b[u", 3); // restore cursor position
      write(1, s2, p2-s2);
      if (hi > 0 && istty > 0) write(1, norm_str, strlen(norm_str));
      if (istty) {
        write(STDOUT_FILENO, "\x1b[s", 3); // save cursor position
        //cmdline_setcur();
      }
    }
    if (!opt_logfile_disabled && !log_stop_flag && logfd >= 0) {
      //write(logfd, s2, p2-s2);
      log_add_to_buffer(s2, p2-s2);
    }
    //free(s2);
  }
}


#define ptlogf(...)  _ptlog(cur_trd, __VA_ARGS__)
//#define ptlogf(...)  ((void)0)

#define conlogf(...)  do { \
  int otty = opt_logtty_disabled; \
  int ofile = opt_logfile_disabled; \
  int olog = opt_log_disabled; \
  opt_logtty_disabled = 0; \
  opt_logfile_disabled = 1; \
  opt_log_disabled = 0; \
  _ptlog(cur_trd, __VA_ARGS__); \
  opt_logtty_disabled = otty; \
  opt_logfile_disabled = ofile; \
  opt_log_disabled = olog; \
} while (0)
