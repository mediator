#include "crackurl.h"

#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void cracked_url_clear (cracked_url_t *url) {
  if (url != NULL) {
    if (url->scheme != NULL) free(url->scheme);
    if (url->host != NULL) free(url->host);
    if (url->portstr != NULL) free(url->portstr);
    if (url->path != NULL) free(url->path);
    if (url->query != NULL) free(url->query);
    if (url->fragment != NULL) free(url->fragment);
    if (url->username != NULL) free(url->username);
    if (url->password != NULL) free(url->password);
    memset(url, 0, sizeof(*url));
  }
}


int crack_url (cracked_url_t *url, const char *urlstr) {
  const char *p, *p1;

  if (url != NULL) memset(url, 0, sizeof(*url));
  if (url == NULL || urlstr == NULL || !urlstr[0]) return -1;

  /* parse scheme */
  p = strstr(urlstr, "://");
  if (p == NULL) {
    /* there seems to be no scheme, assume http */
    if ((url->scheme = strdup("http")) == NULL) goto error;
    url->port = 80; /* default port */
  } else {
    if ((url->scheme = strndup(urlstr, p-urlstr)) == NULL) goto error;
    urlstr = p+3; /* skip scheme and delimiters */
    for (char *t = url->scheme; *t; ++t) *t = tolower(*t);
    if (strcmp(url->scheme, "http") == 0) url->port = 80;
    else if (strcmp(url->scheme, "https") == 0) url->port = 443;
    else if (strcmp(url->scheme, "ftp") == 0) url->port = 21;
    else url->port = 0;
  }

  /* parse host part */
  if (urlstr[0] == 0) goto error; /* no host */
  if ((p = strchr(urlstr, '/')) == NULL) p = urlstr+strlen(urlstr);

  /* check if we have port here */
  for (p1 = p-1; p1 > urlstr && isdigit(*p1); --p1) ;
  if (p1 > urlstr && p1 < p-1 && *p1 == ':') {
    /* got ":digits" */
    int n = 0;
    for (const char *t = p1+1; t < p; ++t) {
      n = n*10+t[0]-'0';
      if (n > 65535) break;
    }
    if (n > 1 && n < 65536) url->port = n;
    if ((url->portstr = strndup(p1, p-p1)) == NULL) goto error;
    p = p1; /* will be fixed later */
    if (p == urlstr) goto error; /* no host name */
  }

  /* parse "name:password@" */
  p1 = strchr(urlstr, '@');
  if (p1 != NULL && p1 < p) {
    if (p1 > urlstr) {
      char *cp = strchr(urlstr, ':');
      if (cp != NULL && cp > p1) cp = NULL;
      if (cp != NULL) {
        /* have both parts */
        if ((url->username = strndup(urlstr, cp-urlstr)) == NULL) goto error;
        if ((url->password = strndup(cp+1, p1-(cp+1))) == NULL) goto error;
      } else {
        /* only username */
        if ((url->username = strndup(urlstr, p1-urlstr)) == NULL) goto error;
      }
    }
    urlstr = p1+1;
  }

  /* parse hostname */
  if (p == urlstr) goto error; /* empty hostname is not allowed */
  if ((url->host = strndup(urlstr, p-urlstr)) == NULL) goto error;
  /* skip it all */
  urlstr = strchr(p, '/');
  if (urlstr == NULL) goto done; /* there is no path */

  /* check if we have query or fragment */
  p = strpbrk(urlstr, "?#");
  if (p == NULL) {
    /* just a path */
    if ((url->path = strdup(urlstr)) == NULL) goto error;
    goto done;
  }
  /* have other parts besides path */
  if ((url->path = strndup(urlstr, p-urlstr)) == NULL) goto error;
  urlstr = p;
  if (*p == '#') {
    /* only fragment */
    if ((url->fragment = strdup(urlstr)) == NULL) goto error;
    goto done;
  }
  /* query and possible fragment */
  if ((p = strchr(urlstr, '#')) == NULL) p = urlstr+strlen(urlstr);
  if ((url->query = strndup(urlstr, p-urlstr)) == NULL) goto error;
  if (*p) {
    if ((url->fragment = strdup(p)) == NULL) goto error;
  }
done:
  return 0;
error:
  cracked_url_clear(url);
  return -1;
}
