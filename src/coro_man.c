////////////////////////////////////////////////////////////////////////////////
#define MAX_BUF_SIZE    (8192)
#define MAX_HDR_SIZE    (5120)


static coro_info_t *coro_list = NULL; // used
static coro_info_t *coro_list_free = NULL; // unused
static coro_info_t *cur_trd = NULL;
static int coro_count = 0;
static coro_context ctx_sheduler;


static coro_info_t *coro_new (coro_func cof) {
  coro_info_t *co;
  if (coro_list_free == NULL) {
    // create new coro
    static int tid = 1;
    co = calloc(1, sizeof(*co)); //FIXME: check for errors
    if (co == NULL) {
      ptlogf("FATAL: can't allocate coro_info_t!\n");
      abort();
    }
    // allocate new stack
    co->tid = tid++;
    coro_stack_alloc(&co->stk, 8192); // 32kb on 32-bit arch
    if (co->stk.sptr == NULL) {
      ptlogf("FATAL: can't allocate stack!\n");
      abort();
    }
    //fprintf(stderr, "new stack at %p, size=%u\n", coro_list[f].stk.sptr, coro_list[f].stk.ssze);
    // allocate both buffers here, we will use 'em eventually
    for (int f = 0; f <= 1; ++f) {
      if ((co->bufs[f].buf = malloc(MAX_BUF_SIZE)) == NULL) {
        ptlogf("FATAL: can't allocate buf[%d]!\n", f);
        abort();
      }
    }
  } else {
    co = coro_list_free;
    coro_list_free = co->next;
    if (co->clifd != -666) coro_destroy(&co->ctx); // no-op, heh
  }
  co->bufs[0].size = co->bufs[1].size = MAX_BUF_SIZE;
  co->bufs[0].used = co->bufs[1].used = 0;
  co->bufs[0].total = co->bufs[1].total = 0;
  for (int f = 0; f < MAX_FDWAITS; ++f) { co->waits[f].fd = co->wres[f].fd = -1; }
  co->clifd = -1;
  co->resolving_name = 0;
  co->rc_state = 0;
  co->prev = NULL;
  co->next = coro_list;
  if (coro_list != NULL) coro_list->prev = co;
  coro_list = co;
  coro_create(&co->ctx, cof, co, co->stk.sptr, co->stk.ssze);
  ++coro_count;
  return co;
}


static void coro_kill (coro_info_t *trd) {
  if (trd != NULL) {
    --coro_count;
    ptlogf("coro_kill: clifd=%d\n", trd->clifd);
    ptlogf("*** dead connection! (workers=%d)\n", coro_count);
    trd->clifd = -1;
    trd->resolving_name = 0;
    for (int f = 0; f < MAX_FDWAITS; ++f) { trd->waits[f].fd = trd->wres[f].fd = -1; }
    if (trd->prev != NULL) trd->prev->next = trd->next; else coro_list = trd->next;
    if (trd->next != NULL) trd->next->prev = trd->prev;
    trd->next = coro_list_free;
    coro_list_free = trd;
    cur_trd = NULL;
    coro_transfer(&trd->ctx, &ctx_sheduler);
  }
}


static void coro_yield (void) {
  coro_context *ctx;
  if (cur_trd == NULL) {
    ptlogf("INTERNAL ERROR: yield() from sheduler!\n");
    abort();
  }
  ctx = &cur_trd->ctx;
  cur_trd = NULL;
  coro_transfer(ctx, &ctx_sheduler);
}
