/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#ifndef _K8CLOCK_H_
#define _K8CLOCK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <time.h>


typedef enum {
  K8CLOCK_ERROR = -1,
  K8CLOCK_HPET = 0,
  K8CLOCK_OTHER = 1
} k8clock_type;


typedef struct {
  struct timespec stt;
} k8clock_t;


extern k8clock_type k8clock_initialized;


extern void k8clock_init (k8clock_t *clk);
extern uint64_t k8clock_ms (k8clock_t *clk); // milliseconds; (0: no timer available)
extern uint64_t k8clock_micro (k8clock_t *clk); // microseconds; (0: no timer available)

extern uint64_t k8clock (void); // milliseconds; (0: no timer available)
extern uint64_t k8clockmicro (void); // microseconds; (0: no timer available)


#ifdef __cplusplus
}
#endif
#endif
